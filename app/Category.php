<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $fillable=['name_en', 'name_ar', 'icon', 'status'];

    public function getIconAttribute($value)
    {
        if ($value) {
            return asset('uploads/categories/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setIconAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/categories/'),$imageName);
            $this->attributes['icon']=$imageName;
        }
    }

    public function orders()
    {
        return $this->hasManyThrough(OrderSubcategory::class, Subcategory::class,'unit_id');
    }

    public function subategories()
    {
        return $this->hasMany('App\Subcategory');
    }
}

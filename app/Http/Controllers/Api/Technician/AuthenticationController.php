<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\Api\BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Notifications;
use App\RateTechnician;
use App\Technician;

class AuthenticationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'firebase_token' => 'required',
            'phone' => 'required_if:phone,p',
            'name'=>'required_if:name,n',
            'email'=>'required_if:email,e',
            'password'=>'required',
            'lat'=>'required',
            'lng'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (auth()->attempt(['phone' => $request->input('key'),
                'password' => $request->input('password')]) || auth()->attempt(['name' => $request->input('key'),
                'password' => $request->input('password')])
        || auth()->attempt(['email' => $request->input('key'),
                'password' => $request->input('password')])
        ) {
            $user = auth()->user();
             if($user->user_type=='technician')
             {
            $verify_code = rand(1111, 9999);
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['firebase_token'] =$request->firebase_token;
            $data['address'] = $user['address'];
            $data['lat'] = $request->lat;
            $data['lng'] = $request->lng;

            $countTechnicianNotifications=Notifications::select('id')->where('user_id',$user->id)->count();
             $data['notifications_count']=$countTechnicianNotifications;

             $userId=User::where('id',$user->id)->pluck('id')->first();
             $getTechnicianId=Technician::where('user_id',$userId)->pluck('id')->first();

            $getAverageRatingOfStationReviews=RateTechnician::where('technician_id',$getTechnicianId)->select('rate')->avg('rate');
            $data['rate'] =(string) round($getAverageRatingOfStationReviews,2);


            if($user->user_status=='0')
            {
                if($user){
                    $user->verify_code=$verify_code;
                    $user->save();
                }
                return response()->json([
                    'status'=>303,
                    'message'=>trans('api.code not active'),
                    'verify_code' => $verify_code,
                ]);
            }
            if($user) {
                $user->firebase_token = $request->firebase_token;
                $user->save();
                return response()->json([
                    'status'=>200,
                    'message'=>trans('api.logged'),
                    'data'=>$data
                ]);
            }}else{
              return response()->json([
                'status'=>404,
                'message'=>trans('api.sorry you are not technician'),
            ]);
            }
        } else {
            return response()->json([
                'status'=>401,
                'message'=>trans('api.somethingwentwrong'),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required',
        ]);
        $user = User::where('phone',  $request->input('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>trans('api.code'),
                'status'=>200,
            ];
            return \Response::json($response,200);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>trans('api.phone not found'),
                'status'=>402,
            ];
            return \Response::json($response,402);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required',
            'lat'=>'required',
            'lng'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code="null";
            $user->user_status=1;
            $user->save();
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['firebase_token'] = $user['firebase_token'];
            $data['lat'] = $request->lat;
            $data['lng'] = $request->lng;

              $countTechnicianNotifications=Notifications::select('id')->where('user_id',$user->id)->count();
             $data['notifications_count']=$countTechnicianNotifications;

             $userId=User::where('id',$user->id)->pluck('id')->first();
             $getTechnicianId=Technician::where('user_id',$userId)->pluck('id')->first();

            $getAverageRatingOfStationReviews=RateTechnician::where('technician_id',$getTechnicianId)->select('rate')->avg('rate');
            $data['rate'] =(string) round($getAverageRatingOfStationReviews,2);

            $response=[
                'message'=>trans('api.verify'),
                'status'=>200,
                'data'=>$data,
            ];

            return \Response::json($response,200);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>trans('api.codenotfound'),
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
        }
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->lat=$request->lat;
        $user->lng=$request->lng;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['address'] = $user['address'];
            $data['lat'] = $user['lat'];
            $data['lng'] = $user['lng'];

              $countTechnicianNotifications=Notifications::select('id')->where('user_id',$user->id)->count();
             $data['notifications_count']=$countTechnicianNotifications;

             $userId=User::where('id',$user->id)->pluck('id')->first();
             $getTechnicianId=Technician::where('user_id',$userId)->pluck('id')->first();

            $getAverageRatingOfStationReviews=RateTechnician::where('technician_id',$getTechnicianId)->select('rate')->avg('rate');
            $data['rate'] =(string) round($getAverageRatingOfStationReviews,2);

            $response=[
                'message'=>trans('api.location accessed successfully'),
                'status'=>200,
                'data'=> $data
            ];
        }
        return \Response::json($response,200);

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

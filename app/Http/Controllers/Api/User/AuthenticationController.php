<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use App\UserSubscription;
use App\Subscription;
use App\Category;

class AuthenticationController extends BaseController
{
    // private function sendMessage($message, $recipients)
    // {
    //     $account_sid = getenv("TWILIO_SID");
    //     $auth_token = getenv("TWILIO_AUTH_TOKEN");
    //     $twilio_number = getenv("TWILIO_NUMBER");
    //     $client = new Client($account_sid, $auth_token);
    //     $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address'=>'required',
            'password' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'social_type' => 'required',
            'social_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>301,
                'message'=>trans('api.submitted'),
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'user_type'=>'user',
            'phone'=>$request->phone,
        ]));
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['jwt_token'] = $user['jwt_token'];
        $data['lat'] =$request->lat;
        $data['lng'] =$request->lng;
        $response=[
            'status'=>200,
            'data'=>$data,
            'message'=>trans('api.logged'),
        ];
       // $this->sendMessage('User registration successful!!', $request->phone);
        return \Response::json($response,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
                $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
    $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $validator = Validator::make($request->all(), [
            'firebase_token' => 'required',
            'phone' => 'required_if:phone,p',
            'name'=>'required_if:name,n',
            'password'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (auth()->attempt(['phone' => $request->input('key'),
                'password' => $request->input('password')]) || auth()->attempt(['name' => $request->input('key'),
                'password' => $request->input('password')]) ) {
            $user = auth()->user();
            $verify_code = rand(1111, 9999);
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['firebase_token'] =$request->firebase_token;
            $data['address'] = $user['address'];
            $data['lat'] = $user['lat'];
            $data['lng'] = $user['lng'];
            
     $subscriptions = UserSubscription::where('user_id', $user->id)->get();


        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            // $res_item['id'] = $res->id;
            $cat=Subscription::where('id',$res->id)->select('category_id')->first();
            $category=Category::select('id','name_'.$lang.' as name')->where('id',$res->id)->first();
            $res_item['category_id'] = $category->id;
            $res_item['id'] = $res->subscription_id;
            $res_item['price'] = $res->total_price;
            $res_item['currency'] = $res->currency;
            $res_list[] = $res_item;
        }
        
        $data['user_subscriptions']=$res_list;
            
            
            if($user->user_status=='0')
            {
                if($user){
                    $user->verify_code=$verify_code;
                    $user->save();
                }
                return response()->json([
                    'status'=>303,
                    'message'=>trans('api.code not active'),
                    'verify_code' => $verify_code,
                ]);
            }
            if($user) {
                $user->firebase_token = $request->firebase_token;
                $user->save();
                return response()->json([
                    'status'=>200,
                    'message'=>trans('api.logged'),
                    'data'=>$data
                ]);
            }
        } else {
            return response()->json([
                'status'=>404,
                'message'=>trans('api.somethingwentwrong'),
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
           if (!$user) {

    return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
}
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->lat=$request->lat;
        $user->lng=$request->lng;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['address'] = $user['address'];
            $data['lat'] = $user['lat'];
            $data['lng'] = $user['lng'];
            $response=[
                'message'=>trans('api.location accessed successfully'),
                'status'=>200,
                'data'=> $data
            ];
        }
        return \Response::json($response,200);

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required',
        ]);
        $user = User::where('phone',  $request->input('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>trans('api.code'),
                'status'=>200,
            ];
            return \Response::json($response,200);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>trans('api.phone not found'),
                'status'=>402,
            ];
            return \Response::json($response,402);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code="null";
            $user->user_status=1;
            $user->save();
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['firebase_token'] = $user['firebase_token'];
            $response=[
                'message'=>trans('api.code'),
                'status'=>200,
                'data'=>$data,
            ];

            return \Response::json($response,200);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>trans('api.codenotfound'),
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token', $jwt)->first();
                 if (!$user) {

    return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
}
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null && ($request->password == $request->password_confirmation)){
            $user->password=$request->password_confirmation;
            if ($user->save())
            {
                $response=[
                    'message'=>trans('api.passwordchangedsuccess'),
                    'status'=>200,
                    'data'=>$user,
                ];
            }
            return \Response::json($response,200);
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>400,
            ];

            return \Response::json($response,401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'social_type'=>'required',
            'social_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user=User::where('email',$request->email)->exists();
        if(!$user)
        {
            return response()->json([
                'status'=>401,
                'message'=>trans('api.user not found'),
                'is_registered'=>false,
            ]);
        }else{
            if($request->social_type == 1) {
                $userData = User::where('email',  $request->email)->pluck('social_token')->first();
                $user = User::where('social_token',  $request->input('social_token'))->first();
            } else {
                $userData = User::where('email',  $request->email)->pluck('social_token')->first();
                $user = User::Where('social_token', $request->input('social_token'))->first();
            }

            if(empty($userData))
            {

                return response()->json([
                    'status'=>200,
                    'message'=>'no tokens found',
                    'is_registered'=>false,

                ]);
            }else{
                if ($user)
                {
                    $verify_code = rand(1111, 9999);
                    $data['id'] = $user['id'];
                    $data['name'] = $user['name'];
                    $data['email'] = $user['email'];
                    $data['phone'] = $user['phone'];
                    $data['image'] = $user['image'];
                    $data['password'] = $user['password'];
                    $data['jwt_token'] = $user['jwt_token'];
                    $data['verify_code'] = $user['verify_code'];
                    $data['firebase_token'] = $user['firebase_token'];
                    $data['notification_status'] = $user['notification_status'];
                    $data['social_token']=$user['social_token'];
                    $data['firebase_token'] =$request->firebase_token;
                    if($user->user_status=='0')
                    {
                        if($user){
                            $user->verify_code=$verify_code;
                            $user->save();
                        }
                        return response()->json([
                            'status'=>303,
                            'message'=>trans('api.code not active'),
                            'verify_code' => $verify_code,
                        ]);
                    }
                    if($user) {
                        $user->firebase_token = $request->firebase_token;
                        $user->save();
                        return response()->json([
                            'status'=>200,
                            'message'=>trans('api.logged'),
                            'data'=>$data,
                            'is_registered'=>true,
                        ]);
                    }
                } else {
                    return response()->json([
                        'status'=>404,
                        'message'=>trans('api.somethingwentwrong'),
                    ]);
                }

            }

        }
    }

        public function getVerfificationCode(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required_if:phone,p',
            'email'=>'required_if:email,e'
        ]);
        $user = User::where('phone',  $request->input('key'))->orWhere('email', $request->input('key'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>trans('api.code'),
                'status'=>200,
            ];
            return \Response::json($response,200);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>trans('api.phone or email not found'),
                'status'=>402,
            ];
            return \Response::json($response,402);
        }
    }
}

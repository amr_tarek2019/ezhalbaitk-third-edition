<?php

namespace App\Http\Controllers\Api\User;

use App\Category;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Order;
use App\OrderSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CategoryForm(Request $request)
    {
 $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $categories=Category::select('id','icon','name_'.$lang. ' as name')
            ->where('status',1)->get();
        $requests_count = OrderSubcategory::count();

        foreach ($categories as $category){
            if ($requests_count > 0) {
                $category->orders_percent = floor((count($category->orders) / $requests_count) * 100) ;
                unset($category->orders);
               
            }
        }
         $data['categories']=$categories;

        if (!empty($data))
        {
            $response=[
                'message'=>'get data of categories successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

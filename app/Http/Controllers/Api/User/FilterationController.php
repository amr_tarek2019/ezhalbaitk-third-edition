<?php

namespace App\Http\Controllers\Api\User;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\RateTechnician;
use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Subcategory;

class FilterationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function FilterForm(Request $request)
    {
    //     $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
    //     $categories=Category::select('id','name_'.$lang. ' as name')
    //         ->where('status',1)->get();





        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
              if (!$user) {
            
    return $response=[
                'success'=>403,
                'message'=>'please login first',
            ]; 
}

            
        
      
            $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
    
            
        if($request->cat_id != null)
        {
        $catTechnicians = Technician::where('category_id',$request->cat_id)->get();
        $res_item = [];
        $res_cat_list  = [];
        foreach ($catTechnicians as $res) {
            $technicianDetails=Technician::where('id',$res->id)->select('id','subcategory_id')->first();
             $res_item['id']=$technicianDetails->id;
             $res_item['subcategory_id']=$technicianDetails->subcategory_id;
             
             $subcategoryData=Subcategory::where('id',$technicianDetails->subcategory_id)->select('name_'.$lang. ' as name')->first();
             $res_item['subcategory_name']=$subcategoryData->name;
             
             
             $ratesData= Technician::where('id',$res->id)->pluck('id')->first();
             
             $Rate=RateTechnician::where('technician_id',$ratesData)->select(DB::raw('round(AVG(rate),0) as rate'))->first();
              $res_item['rate']=$Rate->rate;
             
             
             
             
            $technician = \App\Technician::where('id',$res->id)->select('id','user_id')->first();
            $techDetails=User::where('id',$technician->user_id)->where('user_type','technician')->select('name','image')->first();
              $res_item['technician_name']=$techDetails->name;
            $res_item['technician_image']=$techDetails->image;
            $res_cat_list[] = $res_item;
        }
        $data['technicians_list']=$res_cat_list;
        
        $response=[
            'message'=>'search results',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);
        }
        
                if($request->rate != null)
         {
        $rateings = RateTechnician::where('rate',$request->rate)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($rateings as $res) {
        
           $rate=RateTechnician::where('rate',$res->rate)->select('rate')->avg('rate');
           $res_item['rate']=round($rate);
          $tech_id=Technician::where('id',$res->technician_id)->select('id')->first();
            $res_item['id']= $tech_id->id;
            $technician = \App\Technician::where('id',$res->technician_id)->select('id','user_id')->first();
            
            
            
          $tech_details= \App\User::where('id',$technician->user_id)->where('user_type','technician')->select('name','image')->first();
           
          $res_item['technician_name']=$tech_details->name;
          $res_item['technician_image']=$tech_details->image;
            $res_list[] = $res_item;
            $data['technicians'] = $res_list;
            
        $response=[
            'message'=>'search results',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);
        }
        
  
      



}
        $lng = $request->lng;
        $lat = $request->lat;
   
        
        if($lng !=null && $lat != null)
        {

    
      $haversine = "(6371 * acos(cos(radians($lat))
                        * cos(radians(users.lat))
                        * cos(radians(users.lng)
                        - radians($lng))
                        + sin(radians($lat))
                        * sin(radians(users.lat))))";
     $technician_list= User::select('users.id','users.image',DB::raw('round(avg(rate_technicians.rate)) As rate'),'technicians.subcategory_id','subcategories.name_'.$lang. ' as name')
        ->selectRaw("{$haversine} AS distance")
        ->whereRaw("{$haversine} < ?", [1])->leftJoin('technicians','users.id','technicians.user_id')->
        leftJoin('rate_technicians','technicians.id','rate_technicians.technician_id')
        ->
        leftJoin('subcategories','subcategories.id','technicians.subcategory_id')
         ->get() ;
   
            
            $tech_list = $technician_list;
            $data['technicians']=$tech_list;
          
            
        $response=[
            'message'=>'search results',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

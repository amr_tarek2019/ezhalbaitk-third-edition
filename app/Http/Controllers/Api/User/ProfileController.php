<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\UserSubscription;
use App\Subscription;
use App\Category;

class ProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetProfileDataForm(Request $request)
    {
                $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['address'] = $user['address'];
        $data['lat'] = $user['lat'];
        $data['lng'] = $user['lng'];
        
        $subscriptions = UserSubscription::where('user_id', $user->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $cat=Subscription::where('id',$res->id)->select('category_id')->first();
            $category=Category::select('id','name_'.$lang.' as name')->where('id',$res->id)->first();
            $res_item['category_id'] = $category->id;
            $res_item['id'] = $res->subscription_id;
            $res_item['price'] = $res->total_price;
            $res_item['currency'] = $res->currency;
            $res_list[] = $res_item;
        }
        $data['user_subscriptions']=$res_list;
        
        if ($user)
        {
            $response=[
                'message'=>trans('api.get data of user profile successfully'),
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
          $user = User::where('jwt_token',$jwt)->first();
        if (!$user) {
            
    return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ]; 
}
          $user = User::where('jwt_token',$jwt)->first();
                $check_phone =User::where('phone',$request->phone)->orWhere('email',$request->email)->first();
                if(($check_phone) && ($check_phone['id'] != $user['id'] )) {
                        return $response=[
                'success'=>401, 
                'message'=>trans('api.submitted'),
            ]; 
                } else {
    
        if($request->name!=null){
        $user->name = $request->name;
        }
        if($request->email!=null){
        $user->email=$request->email;
        }
         if($request->phone!=null){
        $user->phone=$request->phone;
         }
         if($request->image!=null){
        $user->image=$request->image;
         }
         if($request->address!=null){
        $user->address=$request->address;
         }
          if($request->lat!=null){
        $user->lat=$request->lat;
         }
          if($request->lng!=null){
        $user->lng=$request->lng;
         }
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['address'] = $user['address'];
            $data['lat'] = $user['lat'];
            $data['lng'] = $user['lng'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['firebase_token'] = $user['firebase_token'];
            $response=[
                'message'=>trans('api.profile changed successfully'),
                'status'=>200,
                'data'=> $data
            ];

        }
         return \Response::json($response,200);
                }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPasswordProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $data['id'] = $user['id'];
        $data['password'] = $user['password'];
        if ($user)
        {
            $response=[
                'message'=>trans('api.get data of user profile successfully'),
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function updatePasswordProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'password'=>'required',
            'old_password'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if($request->password != null && ($request->password == $request->old_password)){
        $user->password = $request->password;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['password'] = $user['password'];
            $response=[
                'message'=>trans('api.profile changed successfully'),
                'status'=>200,
            ];

        }
        return \Response::json($response,200);
        }else{
            return $response=[
                'success'=>404,
                'message'=>'Sorry! Something went wrong.',
            ];
        }
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>trans('api.somethingwentwrong'),
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

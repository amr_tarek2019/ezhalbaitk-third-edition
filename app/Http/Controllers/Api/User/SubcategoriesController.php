<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Unit;

class SubcategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SubcategoriesForm(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $subcategories = Subcategory::where('unit_id', $request->unit_id)->where('status', 1)
            ->get();
        $res_item = [];
        $res_list = [];
        foreach ($subcategories as $res) {
              $res_item['id'] = $res->id;
            $data = Subcategory::where('id', $res->id)->select('name_'.$lang.' as name', 'details_'.$lang.' as details',
                'price','currency')->first();
            $res_item['name']=$data->name;
            $res_item['details']=$data->details;
            $res_item['price']=$data->price;
            $res_item['currency']=$data->currency;
            $res_item['unit_id']= $res->unit_id;
            $dataUnits=Subcategory::where('unit_id', $request->unit_id)->where('status', 1)
            ->pluck('unit_id')->first();
            
            $units=Unit::where('id',$dataUnits)->select('name_'.$lang.' as name','image')->first();
            
             $res_item['unit_name']=$units->name;
             $res_item['unit_image']=$units->image;
            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of subcategories successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Subcategory;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('dashboard.views.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'required',
            'icon' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $category = new Category();
        $category->name_en = $request->name_en;
        $category->name_ar = $request->name_ar;
        $category->icon = $request->icon;
        $category->save();
        return redirect()->route('categories.index')->with('successMsg','Category Updated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('dashboard.views.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name_en = $request->name_en;
        $category->name_ar = $request->name_ar;
        $category->icon = $request->icon;
        $category->save();
        return redirect()->route('categories.index')->with('successMsg','Category Successfully Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::find($id);
        $category->delete();
        return redirect()->back()->with('successMsg','Category Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $category = Category::findOrFail($request->id);
        $category->status = $request->status;
        if($category->save()){
            return 1;
        }
        return 0;
    }

//    public function getCategoryUnits(Request $request)
//    {
//        $units = Unit::where('category_id', $request->category_id)->get();
//        return $units;
//    }
    public function getCategoryUnits($category_id)
    {
        // return $company_id;
        $units = Unit::where('category_id',$category_id)->pluck('category_id','name_en')->toArray();
        return response()->json($units);
    }

    public function getSubcategoriesUnit($unit_id)
    {
        // return $company_id;
        $subcategories = Subcategory::where('unit_id',$unit_id)->pluck('unit_id','name_en')->toArray();
        return response()->json($subcategories);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return view('dashboard.views.city.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'city_en' => 'required',
            'city_ar' => 'required',
        ]);
        $city = new City();
        $city->city_en = $request->city_en;
        $city->city_ar = $request->city_ar;
        $city->save();
        return redirect()->route('cities.index')->with('successMsg','City Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::find($id);
        return view('dashboard.views.city.edit',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::find($id);
        $city->city_en = $request->city_en;
        $city->city_ar = $request->city_ar;
        $city->save();
        return redirect()->route('cities.index')->with('successMsg','City Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city=City::find($id);
        $city->delete();
        return redirect()->back()->with('successMsg','City Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $city = City::findOrFail($request->id);
        $city->status = $request->status;
        if($city->save()){
            return 1;
        }
        return 0;
    }

}

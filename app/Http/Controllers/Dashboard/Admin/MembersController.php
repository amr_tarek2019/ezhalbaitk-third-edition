<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.members.index')->with('users',User::where('user_type','member')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'phone'=>'required'
        ]);
        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
        if ($user){
            return redirect()->route('members.index')->with('successMsg','User Created Before');
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone'=> $request->phone,
            'user_type'=>'member',
            'user_status'=>'1',
            'status'=>'0',
            'verify_code'=>'0',
            'jwt_token'=>'0',
            'latitude'=>'0',
            'longitude'=>'0',
            'facebook_token'=>'0',
            'google_token'=>'0',
            'address'=>'0',
            'firebase_token'=>'0'
        ]);
        //dd($user);

        if ( $user->save())
        {
            return redirect()->route('members.index')->with('successMsg','member Successfully Created');
        }
        return redirect()->route('members.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.views.members.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password =$request->password;
        $user->phone =$request->phone;
        $user->save();
        {
            return redirect()->route('members.index')->with('successMsg','member Successfully Updated');
        }
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if($user)
        {
            return redirect()->route('members.index')->with('successMsg','User Created Before');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('successMsg','User Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->user_status = $request->user_status;
        if($user->save()){
            return 1;
        }
        return 0;
    }
}

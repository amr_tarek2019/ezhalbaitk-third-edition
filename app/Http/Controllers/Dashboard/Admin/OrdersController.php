<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\JobDetails;
use App\Order;
use App\OrderSubcategory;
use App\Setting;
use App\Subcategory;
use App\Technician;
use App\TechnicianOrder;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('dashboard.views.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $orderDetails=OrderSubcategory::where('order_id',$id)->get();

        $data = Order::where('id',$id)->pluck('id');
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');
        $subcategories= Subcategory::whereIn('id',$subcategory_id)->get();

        return view('dashboard.views.orders.show',compact('order','orderDetails','subcategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $technicianUsers=Technician::where('is_busy','0')->pluck('user_id');
        $technicians=User::whereIn('id',$technicianUsers)->where('user_type','technician')->get();
        $order = Order::find($id);
        return view('dashboard.views.orders.edit',compact('order','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'status' => 'required',
            'technician' => 'required',
             'accepted'=>'required',
        ]);

        $getTechnicianId=User::where('id',$request->technician)->pluck('id');
        $technicianId=Technician::whereIn('user_id',$getTechnicianId)->pluck('id')->first();



        $order=Order::where('id',$id)->first();
        $order->status=$request->status;
        $order->save();


        $techOrder=New TechnicianOrder();
        $techOrder->order_id=$order->id;
        $techOrder->technician_id=$technicianId;
        $techOrder->order_number=$order->order_number;
        $techOrder->status= $order->status;
        $techOrder->accepted= $request->accepted;
        $techOrder->save();

        $jobDetails=new JobDetails();
        $jobDetails->user_id=$order->user_id;
        $jobDetails->technician_id=$techOrder->technician_id;
        $jobDetails->order_id=$order->id;
        $jobDetails->status= $order->status;
        $jobDetails->save();


        $technicianStatus = Technician::find($techOrder->technician_id);
        $technicianStatus->is_busy='1';
        $technicianStatus->save();


        return redirect()->route('orders.index')->with('successMsg','Order Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order= Order::find($id);
        $order->delete();
        return redirect()->route('orders.index')->with('successMsg','Order Successfully Deleted');
    }

    function generatePdf($id) {
        $settings=Setting::first();
        $data = Order::findOrFail($id);
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');
        $subcategories= Subcategory::whereIn('id',$subcategory_id)->get();
        $orderSubcategories=OrderSubcategory::where('order_id',$id)->get();
        return view('dashboard.views.orders.invoice',compact('data','settings','subcategories'
        ,'orderSubcategories'));
    }

}

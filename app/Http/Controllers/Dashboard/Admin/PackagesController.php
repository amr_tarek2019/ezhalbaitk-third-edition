<?php

namespace App\Http\Controllers\Dashboard\Admin;


use App\Currency;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('dashboard.views.packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'required',
        ]);
        $package = new Package();
        $package->name_en = $request->name_en;
        $package->name_ar = $request->name_ar;
        $package->save();
        return redirect()->route('packages.index')->with('successMsg','Package Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::find($id);
        return view('dashboard.views.packages.edit',compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package =  Package::find($id);
        $package->name_en = $request->name_en;
        $package->name_ar = $request->name_ar;
        $package->save();
        return redirect()->route('packages.index')->with('successMsg','Package Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package =  Package::find($id);
        $package->delete();
        return redirect()->route('packages.index')->with('successMsg','Package Successfully Deleted');
    }

    public function updateStatus(Request $request)
    {
        $package = Package::findOrFail($request->id);
        $package->status = $request->status;
        if($package->save()){
            return 1;
        }
        return 0;
    }
}

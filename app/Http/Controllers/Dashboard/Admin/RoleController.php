<?php

namespace App\Http\Controllers\Dashboard\Admin;

use Illuminate\Http\Request;
use App\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('dashboard.views.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('permissions')){
            $role = new Role;
            $role->name = $request->name;
            $role->permissions = json_encode($request->permissions);
            if($role->save()){
//                flash(__('Role has been inserted successfully'))->success();
                return redirect()->route('roles.index')->with('successMsg','Role has been inserted successfully');
//                return redirect()->route('');
            }
        }
        //flash(__('Something went wrong'))->error();
        return redirect()->back()->with('errorMsg','Something went wrong');

        //return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail(decrypt($id));
        return view('dashboard.views.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        if($request->has('permissions')){
            $role->name = $request->name;
            $role->permissions = json_encode($request->permissions);
            if($role->save()){
//                flash(__('Role has been updated successfully'))->success();
//                return redirect()->route('roles.index');
                return redirect()->route('roles.index')->with('successMsg','Role has been updated successfully');

            }
        }
//        flash(__('Something went wrong'))->error();
//        return back();
        return redirect()->back()->with('errorMsg','Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Role::destroy($id)){
//            flash(__('Role has been deleted successfully'))->success();
//            return redirect()->route('roles.index');
            return redirect()->route('roles.index')->with('successMsg','Role has been deleted successfully');

        }
        else{
//            flash(__('Something went wrong'))->error();
//            return back();
            return redirect()->back()->with('errorMsg','Something went wrong');

        }
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Subcategory;
use App\Technician;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.technicians.index')->with('users',User::where('user_type','technician')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $units = Unit::all();
        $subcategories=Subcategory::all();
        return view('dashboard.views.technicians.create',compact('categories','subcategories',
        'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'phone'=>'required',
            'category_id' => 'required',
            'unit_id'=> 'required',
            'subcategory' => 'required',
            'address'=>'required',

        ]);
        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
        if ($user){
            return redirect()->route('technicians.index')->with('successMsg','User Created Before');
        }
        $jwt_token = Str::random(25);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone'=>$request->phone,
            'user_type'=>'technician',
            'user_status'=>'0',
            'status'=>'1',
            'verify_code'=>'0',
            'jwt_token'=>$jwt_token,
            'latitude'=>'0',
            'longitude'=>'0',
            'facebook_token'=>'0',
            'google_token'=>'0',
            'address'=>$request->address,
            'firebase_token'=>'0'
        ]);
        $technician=New Technician();
        $technician->user_id=$user->id;
        $technician->category_id = $request->category_id;
        $technician->unit_id = $request->unit_id;
        $technician->subcategory_id=$request->subcategory;
        $technician->is_busy='0';
        if ( $technician->save())
        {
            return redirect()->route('technicians.index')->with('successMsg','technician Successfully Created');
        }
        return redirect()->route('technicians.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $units = Unit::all();
        $subcategories = Subcategory::all();
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();
        return view('dashboard.views.technicians.edit',compact('user','technician',
            'units',
            'categories','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password =$request->password;
        $user->address =$request->address;
        $technician->category_id = $request->category_id;
        $technician->unit_id = $request->unit_id;
        $technician->subcategory_id=$request->subcategory;
        $technician->is_busy=$request->is_busy;
        $user->save();
        $technician->save();
            return redirect()->route('technicians.index')->with('successMsg','User Successfully Updated');


        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if($user)
        {
            return redirect()->route('technicians.index')->with('successMsg','User Created Before');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();
        $user->delete();
        $technician->delete();
        return redirect()->back()->with('successMsg','technician Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->user_status = $request->user_status;
        if($user->save()){
            return 1;
        }
        return 0;
    }

    public function technicianAvailability(Request $request)
    {
        $technician = Technician::findOrFail($request->id);
        $technician->is_busy = $request->is_busy;
        if($technician->save()){
            return 1;
        }
        return 0;
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\UserSubscription;
use App\UserTechnicianSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersSubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = UserSubscription::all();
        return view('dashboard.views.users_subscriptions.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userSubscription = UserSubscription::find($id);
//        $technicianSubscription=UserTechnicianSubscription::where('user_subscription_id',4)->get();

        return view('dashboard.views.users_subscriptions.show', compact('userSubscription'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscription = UserSubscription::find($id);
        $subscription->delete();
        return redirect()->back()->with('successMsg', 'User Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $subscription = UserSubscription::findOrFail($request->id);
        $subscription->status = $request->status;
        if ($subscription->save()) {
            return 1;
        }
        return 0;
    }
}

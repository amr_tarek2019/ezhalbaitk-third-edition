<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class JobDetails extends Model
{
    protected $table='jobs_details';
    protected $fillable=['user_id', 'technician_id', 'order_id', 'status'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}

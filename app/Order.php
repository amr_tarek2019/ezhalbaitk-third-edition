<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable=['user_id', 'city_id','image','note', 'date', 'time', 'lat', 'lng', 'area',
        'block', 'street', 'house', 'floor', 'appartment','order_number',
        'directions', 'status', 'accepted','subscription_id'];

    public function setImageAttribute($value)
    {
        if ($value)
        {
            if(is_file($value)) {
                $imageName = time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/orders/'), $imageName);
                $this->attributes['image'] = $imageName;
            }
            else{
                $this->attributes['image'] = $value;

            }
        }
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/orders/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }

    function subcategory($id) {
        $data = Order::where('id',$id)->pluck('id')->first();
        $subcategory_id = OrderSubcategory::where('id',$data)->pluck('subcategory_id')->first();
        return Subcategory::where('id',$subcategory_id)->first();

    }

    function quantityAndTotal($id)
    {
        $data = Order::where('id',$id)->pluck('id')->first();
        return OrderSubcategory::where('id',$data)->first();
    }
}


<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{

        public static function send($tokens, $msg , $type)
        {

            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  'title' => "te",
                  'sound' => 'default',
                  'message' => $msg,
                  'type' => $type
                ],
                'notification' => [
                    'type'    => $type,
                    'text' => $msg,
                    'title' => "",
                    'sound' => 'default'
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
'AAAAQYVhkmk:APA91bE5FlEcHHnKASxRv9EPUeLfs6TOUN1ViiypoEh6wUA_jJZbCEw05DWot-FctBy1P9w0cgcJs2sF6jPvMzfk5NBsqygCZUtCaguFW_TnQraSUtoL2eRYVioqPVvDzInL3yfJJdxI'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }


        public static function send_details($tokens,$title,$message, $details ,$type='')
        {

           $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  'title' => $title,
                  'sound' => 'default',
                  'message' => $message,
                  'type' => $type
                ],
                'notification' => [
                      'title' => $title,
                    'type'    => $type,
                    'text' => $message,
                    'sound' => 'default'
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            //dd( $fields);
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                'AAAAQYVhkmk:APA91bE5FlEcHHnKASxRv9EPUeLfs6TOUN1ViiypoEh6wUA_jJZbCEw05DWot-FctBy1P9w0cgcJs2sF6jPvMzfk5NBsqygCZUtCaguFW_TnQraSUtoL2eRYVioqPVvDzInL3yfJJdxI'

            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
           // dd( $result);
            return $result;
        }
        public static function send_details_driver($tokens,$title,$message, $details , $q=null,$type='')
        {

            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  // 'title' => $msg,
//                  'title' => $title,
                  'message' => $message,
                  'details' => $details,
                  'type' => $type,
                  'title' => ""
                ],
                'notification' => [
//                    'title' => $title,
                    'message' => $message,
                    'body' => $message,
                    'details' => $details,
                    'type' => $type,
                    'title' => ""
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
                // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
               'AAAAQYVhkmk:APA91bE5FlEcHHnKASxRv9EPUeLfs6TOUN1ViiypoEh6wUA_jJZbCEw05DWot-FctBy1P9w0cgcJs2sF6jPvMzfk5NBsqygCZUtCaguFW_TnQraSUtoL2eRYVioqPVvDzInL3yfJJdxI'

            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
}

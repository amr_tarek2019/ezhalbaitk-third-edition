<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RateTechnician extends Model
{
    protected $table='rate_technicians';
    protected $fillable=[ 'user_id', 'technician_id', 'order_id', 'rate'];


    public function getRateAttribute($value)
{
   return $value = !empty($value) ? $value : 0;
}

  public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function technician()
    {
        return $this->belongsTo('App\Technician','technician_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $table='technicians';
    protected $fillable=['user_id', 'category_id','unit_id' ,'subcategory_id', 'is_busy'];

     public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }
}

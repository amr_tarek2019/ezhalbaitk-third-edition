<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianNote extends Model
{
    protected $table='technicians_notes';
    protected $fillable=['order_id', 'technician_id', 'know_price', 'price'];
}

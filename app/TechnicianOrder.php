<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TechnicianOrder extends Model
{
    protected $table='technicians_orders';
    protected $fillable=['order_id', 'technician_id', 'order_number', 'status', 'accepted'];
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}

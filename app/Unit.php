<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table='units';
    protected $fillable=['id', 'category_id', 'name_en', 'name_ar','warranty_period','period_en'
        ,'period_ar','warranty_text_en','warranty_text_ar','image', 'status'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/units/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/units/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }


    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}

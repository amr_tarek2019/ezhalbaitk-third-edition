<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserTechnicianSubscription extends Model
{
    protected $table='users_technicians_subscriptions';
    protected $fillable=['user_subscription_id', 'technician_id'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function technician()
    {
        return $this->hasMany('App\Technician','technician_id');
    }

}

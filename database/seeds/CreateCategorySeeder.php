<?php

use Illuminate\Database\Seeder;

class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(\Illuminate\Support\Facades\DB::table('categories')->get()->count() == 0){

            \Illuminate\Support\Facades\DB::table('categories')->insert([
                [
                    'name_en' => 'AC',
                    'name_ar' => 'تكييفات',
                    'status' => '0',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'Appliances',
                    'name_ar' => 'اجهزة',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'Carpentry',
                    'name_ar' => 'نجارة',
                    'status' => '0',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],

                [
                    'name_en' => 'Cleaning',
                    'name_ar' => 'تنظيف',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'Electrical',
                    'name_ar' => 'كهرباء',
                    'status' => '0',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'Gardening',
                    'name_ar' => 'الحدائق',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],


            ]);

        }
    }
}

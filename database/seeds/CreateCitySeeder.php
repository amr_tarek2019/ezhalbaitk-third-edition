<?php

use Illuminate\Database\Seeder;

class CreateCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(\Illuminate\Support\Facades\DB::table('cities')->get()->count() == 0){

            \Illuminate\Support\Facades\DB::table('cities')->insert([
                [
                    'city_en' => 'Doha',
                    'city_ar' => 'الدوحة',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_en' => 'Al- Khor',
                    'city_ar' => 'الخور',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_en' => 'Al-Wakra',
                    'city_ar' => 'الوكرة',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],

                [
                    'city_en' => 'Al- Rayyan',
                    'city_ar' => 'الريان',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_en' => 'Al- Shamal',
                    'city_ar' => 'الشمال',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_en' => 'Umm Salal',
                    'city_ar' => 'أم صلال',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],


            ]);

        }
    }
}

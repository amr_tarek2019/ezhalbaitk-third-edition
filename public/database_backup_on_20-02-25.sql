

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_type` tinyint(1) NOT NULL,
  `social_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwt_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `verify_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('1','admin','admin@admin.com','01018472418','','$2y$10$o2d8MUIwUlcV/KK7lewbaOjWO0xCEbRI9TY57ykHnRIuxQBjTBSWe','helwan','','31.212','32.456','0','0','0','123zxcv','1','1','null','admin','QUKEA403TuxxwFpCdC2oIMpX6uz3VEkUcrFCbaNQNjTPdk2SMhSg7cDRUWoy','','2020-02-20 08:01:36');

INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('26','memouser123','atef7374@gmail.com','01121805383','','$2y$10$QHyA5xu6fBXyM3yVB4HuOeiDYxgx23tKa9V1OzBQBRH3D789xYMQq','3 Hashem Al Ashkar, Huckstep, El Nozha, Cairo Governorate, Egypt','1582468557.jpg','30.1259119','31.3751564','0','0','Fq8ZFEDruWZUZ7ZdR0msw4yWZ','cMtKcpEFOps:APA91bEiHQFryakNM8cL4l5FIRkll3XnNYPay_QaT7uQ8DfgbnYeKQJcOspFi9H0RA64Vg__Ug3pEu0Wq5Ig5MeTOrajE7EFDM0c_X9GzNCDOPinOiuiyp88Yiwi04SCzZsTrSndpst5','1','1','null','user','','2020-02-23 11:05:16','2020-02-24 13:20:04');

INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('28','test1235','memo@yahoo.com','01121805384','2020-02-23 00:00:00','$2y$10$QHyA5xu6fBXyM3yVB4HuOeiDYxgx23tKa9V1OzBQBRH3D789xYMQq','add','1582468660.jpg','0.0','0.0','1','ss','Fq8ZFEDruWZUZ7ZdR0msw4yWP','eTL4GyfWddw:APA91bGM56GXX59Lxl6PDPfUpJxi78zzzK_K8J4SRyl3OJso6HmyM4vZnUGLCrzG5jIlGgvmExaFJ1M0-JYMD7fbxyZ5aeUrO92RCr5t8mF_vyKKyugWVuT2GMIG81Ch3J8nBSrl_H6r','1','1','1356','technician','remembertokken','','2020-02-25 12:07:59');

INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('29','hamed','hamedgrand82@gmail.com','01025744857','','$2y$10$UZiDindqtA1/nwMkXTLhoumIaDYAx52EHNFu1O59xUWxj/d1lwbDW','3 هاشم الأشقر، الهايكستب، قسم النزهة، محافظة القاهرة‬، مصر','','30.1259274','31.3750577','0','0','3FYgFRZwsNVFMDIgMoSsQIruL','cMtKcpEFOps:APA91bEiHQFryakNM8cL4l5FIRkll3XnNYPay_QaT7uQ8DfgbnYeKQJcOspFi9H0RA64Vg__Ug3pEu0Wq5Ig5MeTOrajE7EFDM0c_X9GzNCDOPinOiuiyp88Yiwi04SCzZsTrSndpst5','1','1','null','user','','2020-02-24 10:09:47','2020-02-25 11:48:10');

INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('30','HAMDY','hamedgrand1991@gmail.com','01102777062','','$2y$10$yX2l6k/w3h.INgcNr60AROahUmzs1pGsBciof.lpkat/s2X6/Pbja','0','','','','0','','Dy83OddbEuGciN47ETRfoLwfm','eTL4GyfWddw:APA91bGM56GXX59Lxl6PDPfUpJxi78zzzK_K8J4SRyl3OJso6HmyM4vZnUGLCrzG5jIlGgvmExaFJ1M0-JYMD7fbxyZ5aeUrO92RCr5t8mF_vyKKyugWVuT2GMIG81Ch3J8nBSrl_H6r','1','1','0','technician','','2020-02-25 11:38:33','2020-02-25 12:09:57');

INSERT INTO users (id, name, email, phone, email_verified_at, password, address, image, lat, lng, social_type, social_token, jwt_token, firebase_token, user_status, status, verify_code, user_type, remember_token, created_at, updated_at) VALUES ('31','ALI','hamedhigazy82@gmail.com','01143905901','','$2y$10$.U1yEwvkIEmD8a.AdlyYQex0GG5UKuupwpjKMgKP8Qh1RFVtEQAzO','0','','','','0','','8vAb9505B6X8sw0BtlnfQHGHl','eTL4GyfWddw:APA91bGM56GXX59Lxl6PDPfUpJxi78zzzK_K8J4SRyl3OJso6HmyM4vZnUGLCrzG5jIlGgvmExaFJ1M0-JYMD7fbxyZ5aeUrO92RCr5t8mF_vyKKyugWVuT2GMIG81Ch3J8nBSrl_H6r','1','1','0','technician','','2020-02-25 12:20:28','2020-02-25 12:22:35');


CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('1','AC','تكييفات','Untitledd-1.png','1','2020-01-27 08:03:31','2020-01-27 08:03:31');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('2','Appliances','اجهزة','Untitled-1002.png','1','2020-01-27 08:03:31','2020-02-24 13:47:47');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('3','Carpentry','نجارة','Untitled-1020.png','1','2020-01-27 08:03:31','2020-01-27 08:03:31');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('4','Cleaning','تنظيف','Untitled-1020.png','1','2020-01-27 08:03:31','2020-01-27 08:03:31');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('5','Electrical','كهرباء','Untitled-1020.png','1','2020-01-27 08:03:31','2020-01-27 08:03:31');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('6','Gardening','الحدائق','Untitled-1002.png','1','2020-01-27 08:03:31','2020-01-27 08:03:31');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('7','General','عامة','Untitled-1002.png','1','2020-02-04 00:00:00','');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('8','Laundry','مغسلة','Untitled-1002.png','1','2020-02-04 00:00:00','');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('9','Fire Alarm','انذارات الحرائق','Untitledd-1.png','1','2020-02-04 00:00:00','');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('10','Plumbing','السباكة','Untitledd-1.png','1','2020-02-04 00:00:00','');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('11','Pesticides','ابادة الحشرات','Untitledd-1.png','1','2020-02-04 00:00:00','');

INSERT INTO categories (id, name_en, name_ar, icon, status, created_at, updated_at) VALUES ('12','Computer','اجهزة الحاسب','Untitledd-1.png','1','2020-02-04 00:00:00','');


CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `city_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('1','Doha','الدوحة','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('2','Al- Khor','الخور','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('3','Al-Wakra','الوكرة','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('4','Al- Rayyan','الريان','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('5','Al- Shamal','الشمال','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('6','Umm Salal','أم صلال','1','2020-01-27 08:03:41','2020-01-27 08:03:41');

INSERT INTO cities (id, city_en, city_ar, status, created_at, updated_at) VALUES ('7','Cairo','القاهرة','1','2020-02-25 11:17:25','2020-02-25 11:18:10');


CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `problem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacts_user_id_foreign` (`user_id`),
  CONSTRAINT `contacts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_foreign` (`user_id`),
  KEY `notifications_order_id_foreign` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('19','26','56','order status changed successfully','2020-02-23 12:43:19','2020-02-23 12:57:17');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('20','28','56','technician replied on order','2020-02-23 12:56:50','2020-02-23 12:56:50');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('21','26','1','job successfully posted','2020-02-24 07:09:03','2020-02-24 07:09:03');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('22','26','2','job successfully posted','2020-02-24 07:23:52','2020-02-24 07:23:52');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('23','26','3','job successfully posted','2020-02-24 07:24:12','2020-02-24 07:24:12');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('24','26','4','job successfully posted','2020-02-24 07:24:29','2020-02-24 07:24:29');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('25','26','5','job successfully posted','2020-02-24 07:42:24','2020-02-24 07:42:24');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('26','26','6','order status changed successfully','2020-02-24 07:47:39','2020-02-24 08:02:58');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('27','28','6','technician replied on order','2020-02-24 08:02:33','2020-02-24 08:02:33');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('28','26','7','order status changed successfully','2020-02-24 08:07:41','2020-02-24 08:31:01');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('29','28','7','technician replied on order','2020-02-24 08:09:36','2020-02-24 08:09:36');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('30','26','8','order status changed successfully','2020-02-24 08:44:40','2020-02-24 08:53:36');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('31','28','8','technician replied on order','2020-02-24 08:52:57','2020-02-24 08:52:57');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('32','26','9','job successfully posted','2020-02-24 09:53:24','2020-02-24 09:53:24');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('33','26','10','job successfully posted','2020-02-24 10:06:50','2020-02-24 10:06:50');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('34','26','11','job successfully posted','2020-02-24 10:06:53','2020-02-24 10:06:53');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('35','26','12','job successfully posted','2020-02-24 10:07:09','2020-02-24 10:07:09');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('36','26','13','job successfully posted','2020-02-24 10:11:10','2020-02-24 10:11:10');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('37','26','14','job successfully posted','2020-02-24 10:14:47','2020-02-24 10:14:47');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('38','26','15','job successfully posted','2020-02-24 10:21:02','2020-02-24 10:21:02');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('39','26','16','job successfully posted','2020-02-24 10:25:42','2020-02-24 10:25:42');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('40','26','17','job successfully posted','2020-02-24 11:09:11','2020-02-24 11:09:11');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('41','26','18','order status changed successfully','2020-02-24 11:12:25','2020-02-24 11:14:01');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('42','29','19','job successfully posted','2020-02-24 11:35:15','2020-02-24 11:35:15');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('43','29','20','job successfully posted','2020-02-24 11:35:20','2020-02-24 11:35:20');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('44','26','21','job successfully posted','2020-02-24 13:27:28','2020-02-24 13:27:28');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('45','26','22','job successfully posted','2020-02-25 11:50:27','2020-02-25 11:50:27');

INSERT INTO notifications (id, user_id, order_id, text, created_at, updated_at) VALUES ('46','26','23','job successfully posted','2020-02-25 12:25:33','2020-02-25 12:25:33');


CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `payment_type` tinyint(5) NOT NULL,
  `city_id` bigint(20) unsigned NOT NULL,
  `subscription_id` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appartment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QR',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_city_id_foreign` (`city_id`),
  KEY `orders_subscription_id_foreign` (`subscription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO orders (id, user_id, payment_type, city_id, subscription_id, image, note, date, time, lat, lng, area, block, street, house, floor, appartment, directions, status, accepted, order_number, currency, created_at, updated_at) VALUES ('19','29','0','1','','','','26/2/2020','1:30 PM','30.12593','31.3750677','Doha','3','النزهة','1','2','2','test','0','0','150963892','QR','2020-02-24 11:35:15','2020-02-24 11:35:15');

INSERT INTO orders (id, user_id, payment_type, city_id, subscription_id, image, note, date, time, lat, lng, area, block, street, house, floor, appartment, directions, status, accepted, order_number, currency, created_at, updated_at) VALUES ('20','29','0','1','','','','26/2/2020','1:30 PM','30.12593','31.3750677','Doha','3','النزهة','1','2','2','test','0','0','809190634','QR','2020-02-24 11:35:20','2020-02-24 11:35:20');

INSERT INTO orders (id, user_id, payment_type, city_id, subscription_id, image, note, date, time, lat, lng, area, block, street, house, floor, appartment, directions, status, accepted, order_number, currency, created_at, updated_at) VALUES ('21','26','0','1','','','','24/2/2020','3:24 PM','30.1259224','31.3750641','Doha','3','المطار','9','3','النزهة','القاهرة','0','0','249667201','QR','2020-02-24 13:27:28','2020-02-24 13:27:28');

INSERT INTO orders (id, user_id, payment_type, city_id, subscription_id, image, note, date, time, lat, lng, area, block, street, house, floor, appartment, directions, status, accepted, order_number, currency, created_at, updated_at) VALUES ('22','26','0','7','','','','25/2/2020','05:0 PM','30.1259059','31.3750896','القاهرة','3','المطار','22','3','12','بجوار الحناوي','0','0','264902252','QR','2020-02-25 11:50:27','2020-02-25 11:50:27');

INSERT INTO orders (id, user_id, payment_type, city_id, subscription_id, image, note, date, time, lat, lng, area, block, street, house, floor, appartment, directions, status, accepted, order_number, currency, created_at, updated_at) VALUES ('23','26','0','1','','','','25/2/2020','02:26 PM','30.1259192','31.3750937','الدوحة','2','ss','2','2','gg','gd','0','0','687040963','QR','2020-02-25 12:25:33','2020-02-25 12:25:33');


CREATE TABLE `orders_rate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `technician_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_rate_technician_id_foreign` (`technician_id`),
  KEY `orders_rate_order_id_foreign` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO orders_rate (id, technician_id, order_id, rate, created_at, updated_at) VALUES ('13','8','56','4.5','2020-02-23 12:58:07','2020-02-23 12:58:07');

INSERT INTO orders_rate (id, technician_id, order_id, rate, created_at, updated_at) VALUES ('14','8','6','4.5','2020-02-24 08:04:01','2020-02-24 08:04:01');

INSERT INTO orders_rate (id, technician_id, order_id, rate, created_at, updated_at) VALUES ('15','8','7','5','2020-02-24 08:35:25','2020-02-24 08:35:25');

INSERT INTO orders_rate (id, technician_id, order_id, rate, created_at, updated_at) VALUES ('16','8','18','2','2020-02-24 11:20:12','2020-02-24 11:20:12');


CREATE TABLE `packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO packages (id, name_en, name_ar, status, created_at, updated_at) VALUES ('1','platinum','بلاتين','1','2020-01-27 08:03:50','2020-01-27 08:03:50');

INSERT INTO packages (id, name_en, name_ar, status, created_at, updated_at) VALUES ('2','gold','ذهب','1','2020-01-27 08:03:50','2020-01-27 08:03:50');

INSERT INTO packages (id, name_en, name_ar, status, created_at, updated_at) VALUES ('3','silver','فضة','1','2020-01-27 08:03:50','2020-01-27 08:03:50');


CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `subcategories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unit_id` bigint(20) unsigned NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QR',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_unit_id_foreign` (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('16','1','Air condition check','فحص التكييف','Detecting and inspecting all types of adaptations, and checking for faults
Checking the conditioner for malfunctions and determining the problem or parts required to be changed
We offer you a group of professional technicians, for the maintenance of all types of air-conditioners. Maintenance of Sharp air conditioning, Union Air, Power, Desert air conditioning, and the rest of the air conditioners.','كشف ومعاينة جميع أنواع التكييفات ، و الكشف على الأعطال
فحص التكييف من الأعطال وتحديد المشكلة او القطع المطلوبة تغييرها
نقدم لك مجموعة من الفنين المحترفين ، لصيانة جميع انواع التكييفات صيانة تكييف شارب وكارير ويونيون اير وباور وجرى والتكييف الصحراوى وباقى انواع التكييف','','1','QR','2020-02-16 14:12:39','2020-02-25 11:27:41');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('18','1','Air conditioning installation','تركيب تكييف','Air conditioning installation service, contracted with professional technicians, installation of all types of air conditioning','خدمة تركيب التكييف بالتعاقد مع فنيين محترفين ، تركيب جميع انواع التكييف','400','1','QR','2020-02-16 14:19:46','2020-02-24 13:20:28');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('19','1','Air conditioning cleaning and summer maintenance','تنظيف التكييف وصيانة الصيف','Check your air conditioning before running, and prepare it for the summer period. Clean the air conditioning to start the summer season, and it includes:
Dismantle and clean the filter
Decipher the face
- Disassemble the basin and clean it with a crystal to set the speed
fan
Clean the outdoor unit with a crystal (or water, if available).
Freon measurement (not including Freon shipping, you can review Freon shipping prices on the website)
Check the conditioner to prepare it to operate normally','افحص تكييفك قبل التشغيل ، وجهزه لفترة الصيف ، تنظيف التكييف لبدء موسم الصيف ، ويشمل: 
- فك الفلتر وتنظيفه
- فك الوجه
- فك الحوض وتنظيفه بالبلاور لظبط سرعة
المروحة
- تنظيف الوحدة الخارجية بالبلاور (او الماء ان اتيحت الفرصة)
- قياس الفريون ( غير شامل شحن فريون ، يمكنك مراجعة اسعار شحن الفريون على الموقع ) 
- فحص التكييف لتجهيزه للعمل بشكل طبيعى','120','1','QR','2020-02-16 14:22:00','2020-02-24 13:20:32');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('20','1','painting walls','اعمال نقاشة ، دهان حوائط','Painting the walls of houses and villas, we offer all modern discussion and paints in a professional manner, and modern decoration works
The implementation is carried out by a group of professional technicians in the work of establishing the discussion and finishes, for all rooms of the house and the implementation of modern decoration works and all kinds of modern paints','دهان حوائط المنازل والفيلات ، نقدم جميع اعمال النقاشة والدهانات العصرية بطريقة محترفة ، وأعمال الديكور الحديث
يقوم بالتنفيذ مجموعة من الفنيين المحترفين فى أعمال وتأسيس النقاشة والتشطبيات ، لجميع غرف المنزل و وتنفيذ اعمال الديكور الحديث وجميع انواع الدهانات الحديثة','','1','QR','2020-02-16 14:28:51','2020-02-25 11:27:28');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('21','2','Complete finishing paint by meter','تشطيب نقاشة كامل وديكورات بالمتر','A complete paint finishing for apartments, villas and stores
1 water seal face +3 paste face + 2 face blanket + 2 finishing faces + decorations','تشطيب نقاشة كامل للشقق والفيلات والمحلات 
1وجه سيلر مائى +3وجه معجون+2وجه بطانه+2وجه تشطيب+ديكورات','30 جنيه للمتر','1','QR','2020-02-16 14:35:20','2020-02-24 13:20:38');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('24','1','Freon charging air condition','شحن فريون التكييف','Freon inspection, shipment, inspection and control service before operation, in contract with professional technicians, freon charging for all types of air conditioning','خدمة الكشف على الفريون وشحنه وفحصه وظبطه قبل التشغيل ، بالتعاقد مع فنيين محترفين ، شحن الفريون لجميع انواع التكييف','300','1','QR','2020-02-24 14:07:12','2020-02-24 14:07:22');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('27','20','ROOF PROTECTION','عزل الاسطح','Surface insulation services using the latest technologies and the best equipment with professional hands in a field
Insulate surfaces and walls','خدمات عزل الاسطح باستخدام احدث التقنيات وأجود المعدات بأيادي فنين مختصين في مجال
عزل الاسطح والجدران','2000','1','QR','2020-02-25 11:13:36','2020-02-25 11:13:54');

INSERT INTO subcategories (id, unit_id, name_en, name_ar, details_en, details_ar, price, status, currency, created_at, updated_at) VALUES ('28','17','Networking','الشبكات','Local network services and servers
Network cable installation and installation','خدمات الشبكات المحلية والسيرفرات
تركيب وانشاء كابلات الشبكات','100','1','QR','2020-02-25 12:16:51','2020-02-25 12:16:57');


CREATE TABLE `subscriptions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `months` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QR',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_category_id_foreign` (`category_id`),
  KEY `subscriptions_package_id_foreign` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO subscriptions (id, category_id, package_id, months, price, status, currency, created_at, updated_at) VALUES ('3','1','1','5','1000','1','QR','2020-02-24 00:00:00','2020-02-28 00:00:00');


CREATE TABLE `technicians` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `subcategory_id` bigint(20) unsigned NOT NULL,
  `is_busy` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `technicians_user_id_foreign` (`user_id`),
  KEY `technicians_category_id_foreign` (`category_id`),
  KEY `technicians_subcategory_id_foreign` (`subcategory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO technicians (id, user_id, category_id, subcategory_id, is_busy, created_at, updated_at) VALUES ('9','30','1','24','1','2020-02-25 11:38:33','2020-02-25 11:40:13');

INSERT INTO technicians (id, user_id, category_id, subcategory_id, is_busy, created_at, updated_at) VALUES ('10','31','12','28','1','2020-02-25 12:20:28','2020-02-25 12:22:35');


CREATE TABLE `technicians_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `technician_id` bigint(20) unsigned NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `accepted` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `technicians_orders_order_id_foreign` (`order_id`),
  KEY `technicians_orders_technician_id_foreign` (`technician_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO migrations (id, migration, batch) VALUES ('1','2014_10_12_000000_create_users_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('2','2014_10_12_100000_create_password_resets_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('3','2020_01_22_144458_create_categories_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('5','2020_01_22_152142_create_cities_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('6','2020_01_23_080318_create_settings_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('7','2020_01_23_080723_create_packages_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('8','2020_01_23_081202_create_subscriptions_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('9','2020_01_23_081606_create_contacts_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('10','2020_01_23_083503_create_technicians_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('11','2020_01_23_084334_create_users_subscriptions_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('12','2020_01_23_093227_create_users_technicians_subscriptions','1');

INSERT INTO migrations (id, migration, batch) VALUES ('13','2020_01_23_093655_create_rate_technicians_subscriptions','1');

INSERT INTO migrations (id, migration, batch) VALUES ('15','2020_01_23_134350_create_subscriptions_categories_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('17','2020_01_23_190937_create_jobs_details_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('18','2020_01_23_191403_create_rate_technicians_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('19','2020_01_23_194942_create_notifications_table','1');

INSERT INTO migrations (id, migration, batch) VALUES ('22','2020_01_27_082538_create_order_subcategory_table','3');

INSERT INTO migrations (id, migration, batch) VALUES ('25','2020_01_27_144340_create_units_table','5');

INSERT INTO migrations (id, migration, batch) VALUES ('26','2020_01_22_151516_create_subcategories_table','6');

INSERT INTO migrations (id, migration, batch) VALUES ('27','2020_02_11_103037_create_technicians_orders_table','7');

INSERT INTO migrations (id, migration, batch) VALUES ('32','2020_02_12_092048_create_technicians_notes_table','8');

INSERT INTO migrations (id, migration, batch) VALUES ('33','2020_02_12_111800_create_order_price_amount_table','9');

INSERT INTO migrations (id, migration, batch) VALUES ('34','2020_02_12_113031_create_orders_rate_table','10');

INSERT INTO migrations (id, migration, batch) VALUES ('35','2020_01_27_081915_create_orders_table','11');

<?php
return[
    'addcity'=>'اضف مدينة',
    'cities'=>'المدن',
    'completeform'=>'اكمل البيانات',
    'nameenglish'=>'اسم المدينة باللغة الانجليزية',
    'namearabic'=>'اسم المدينة باللغة العربية',
    'submitForm'=>'اخضع',
    'editcity'=>'تعديل مدينة',
    'citiestable'=>'جدول المدن',
    'addnew'=>'اضف جديد',
    'citiesdetails'=>'تفاصيل المدن',
    'status'=>'الحالة',
    'actions'=>'الاجراءات',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'changemsgsuccess'=>'تم تغيير الحالة بنجاح',
    'changemsgfailed'=>'حدث خطأ ما',
    'lat'=>'خط العرض',
    'lng'=>'خط الطول'
];
<?php
return [
    'notifications'=>'الاشعارات',
    'addnew'=>'اضف جديد',
    'notificationstable'=>'جدول الاشعارات',
    'username'=>'اسم المستخدم',
    'title'=>'العنوان',
    'text'=>'نص الاشعار',
    'created'=>'انشئ في',
    'createnotification'=>'انشاء اشعار',
    'addnotification'=>'اضافة جديد',
    'submit'=>'اخضع',
    'cancel'=>'الغاء',
    'delete'=>'حذف'
];
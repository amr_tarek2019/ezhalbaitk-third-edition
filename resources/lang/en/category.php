<?php
return[
    'categoriesdatatable'=>'categories data table',
    'titlearabic'=>'arabic name',
    'titleenglish'=>'english name',
    'image'=>'image',
    'createdat'=>'created at',
    'updatedat'=>'updated at',
    'actions'=>'actions',
    'edit'=>'edit',
    'delete'=>'delete',
    'submit'=>'submit',
    'uploadfile'=>'upload file',
    'editcategory'=>'edit category',
    'category'=>'category',
    'addcategory'=>'add category',
    'categoriesdata'=>'categories data',
    'categories'=>'categories',
    'completeform'=>'complete form',
    'status'=>'status',
    'deletemsg'=>'Are you sure? You want to delete this?',
    'successchangestatus'=>'success , category status updated successfully',
    'errorchangestatus'=>'danger , Something went wrong'
];
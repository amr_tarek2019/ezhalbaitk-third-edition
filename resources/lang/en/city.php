<?php
return[
    'addcity'=>'Add City',
    'cities'=>'Cities',
    'completeform'=>'Complete Form',
    'nameenglish'=>'name english',
    'namearabic'=>'name arabic',
    'submitForm'=>'Submit form',
    'editcity'=>'edit city',
    'citiestable'=>'Cities DataTable',
    'addnew'=>'add new',
    'citiesdetails'=>'Cities Details',
    'status'=>'status',
    'actions'=>'actions',
    'edit'=>'edit',
    'delete'=>'delete',
    'changemsgsuccess'=>'success , city status updated successfully',
    'changemsgfailed'=>'something went wrong',
    'lat'=>'Latitude',
    'lng'=>'Longitude'
];
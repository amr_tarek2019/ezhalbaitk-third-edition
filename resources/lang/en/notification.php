<?php
return[
    'notifications'=>'notifications',
    'addnew'=>'add new',
    'notificationstable'=>'notifications table',
    'username'=>'user name',
    'title'=>'title',
    'text'=>'text',
    'created'=>'created at',
    'createnotification'=>'create notification',
    'addnotification'=>'add notification',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'delete'=>'delete'
];
<?php
return [
    'AddPackage'=>'Add Package',
    'Packages'=>'Packages',
    'CompleteForm'=>'Complete Form',
    'nameenglish'=>'name english',
    'namearabic'=>'name arabic',
    'period'=>'period',
    'price'=>'price',
    'total'=>'total',
    'Submit'=>'Submit',
    'editpackage'=>'edit package ',
    'PackagesDataTable'=>'Packages DataTable',
    'AddNew'=>'Add New',
    'PackagesDetails'=>'Packages Details',
    'actions'=>'actions',
    'edit'=>'edit',
    'delete'=>'delete',
    'changemsgsuccess'=>'success , city status updated successfully',
    'changemsgfailed'=>'something went wrong',
    'currency'=>'currency'

];

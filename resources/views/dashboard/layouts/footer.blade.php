<!-- Footer -->
<footer id="footer">
    <div class="container">

        <!-- footer socials -->
        <div class="row">

            <div class="footer_socials col-sm-12 text-center">

                <div class="contact_icons">

                </div>

                <div class=“site-copyright”>
                    <p align="center"> <a href="http://2grand.net/" target=“_blank” class=“grand”><img src="{{ asset('assets/dashboard/images/grandandroid.png') }}" width="100px"></a> جميع الحقوق محفوظة لشركة جراند ©</p>
                </div>
            </div>

        </div>
        <!-- end footer socials -->

    </div>
    <!-- end container -->
</footer>
<!-- end footer -->

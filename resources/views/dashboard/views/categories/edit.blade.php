@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>edit category</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">category</li>
                                <li class="breadcrumb-item active">edit category</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5> complete form </h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('categories.update',$category->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">english name</label>
                                        <input class="form-control" value="{{$category->name_en}}" id="name_en" name="name_en" placeholder="english name"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">arabic name</label>
                                        <input class="form-control" value="{{$category->name_ar}}" id="name_ar" name="name_ar"  placeholder="arabic name"/>
                                    </div>
                                </div>
                                <br>
                                <div style="margin-left: 15px;">
                                    <img class="img-responsive img-thumbnail" src="{{ asset($category->icon) }}" style="height: 100px; width: 100px;" alt="">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="form-label" style="margin-left: 30px;">upload file</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="icon" name="icon" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

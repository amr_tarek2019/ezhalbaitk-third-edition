@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>ezhalbaitk</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-7 xl-100">
                <div class="row">
                    <div class="owl-carousel owl-theme" id="owl-carousel-14">
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="users"></i>
                                    <div><span>Total Users</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\User::where('user_type', 'user')->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="users"></i>
                                    <div><span>Total Technicians</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\User::where('user_type', 'technician')->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="users"></i>
                                    <div><span>Total Admins</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\User::where('user_type', 'admin')->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="shopping-cart"></i>
                                    <div><span>Total Orders</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\Order::all()->count() }}</h4>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="list"></i>
                                    <div><span>Total Categories</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\Category::all()->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="list"></i>
                                    <div><span>Total Units</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\Unit::all()->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="list"></i>
                                    <div><span>Total Subcategories</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\Subcategory::all()->count() }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <div class="card-body ecommerce-icons text-center"><i data-feather="package"></i>
                                    <div><span>Total Packages</span></div>
                                    <h4 class="font-primary mb-0 counter">{{ \App\Package::all()->count() }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Total Users</h5>
                            </div>
                            <div class="card-body charts-box">
                                <div class="flot-chart-container">
                                    <div class="flot-chart-placeholder" id="userGraph"></div>
                                </div>
                                <div class="code-box-copy">
                                    <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                    <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Total Technicians</h5>
                    </div>
                    <div class="card-body charts-box">
                        <div class="flot-chart-container">
                            <div class="flot-chart-placeholder" id="technicianGraph"></div>
                        </div>
                        <div class="code-box-copy">
                            <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                            <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Total Orders</h5>
                    </div>
                    <div class="card-body charts-box">
                        <div class="flot-chart-container">
                            <div class="flot-chart-placeholder" id="orderGraph"></div>
                        </div>
                        <div class="code-box-copy">
                            <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                            <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-xl-5 xl-100">
                <div class="card">
                    <div class="card-header">
                        <h5>Last 5 Suggestions</h5>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive sellers">
                            <table class="table table-bordernone">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">phone</th>
                                    <th scope="col">problem</th>
                                    <th scope="col">Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach (\App\Contact::orderBy('id', 'desc')->take(5)->get() as $key => $contact)
                                <tr>
                                    <td>
                                        <div class="d-inline-block align-middle">
                                            <img class="img-radius img-50 align-top m-r-15 rounded-circle" src="{{ asset($contact->user->image) }}" alt="">
                                            <div class="d-inline-block">
                                                <p>{{$contact->user->name}}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p>{{$contact->user->phone}}</p>
                                    </td>
                                    <td>
                                        <p>{{$contact->problem}}</p>
                                    </td>
                                    <td>
                                        <p>
                                         <a href="{{ route('suggestions.show',$contact->id) }}" class="btn btn-info btn-sm"><i class="material-icons">show</i></a>
                                        </p>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="code-box-copy">
                            <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                            <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class="table-responsive sellers"&gt;
  &lt;table class="table table-bordernone"&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope="col"&gt;Name&lt;/th&gt;
        &lt;th scope="col"&gt;Sale&lt;/th&gt;
        &lt;th scope="col"&gt;Stock&lt;/th&gt;
        &lt;th scope="col"&gt;Categories&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/6.jpg') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/2.png') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/3.jpg') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/4.jpg') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/5.jpg') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="{{ asset('assets/dashboard/images/user/15.png') }}" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                        </div>
                    </div>
                </div>
            </div>




            <div class="col-xl-5 xl-100">
                <div class="card">
                    <div class="card-header">
                        <h5>Last 5 Orders</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive sellers">
                            <table class="table table-bordernone">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach (\App\Order::orderBy('id', 'desc')->take(5)->get() as $key => $order)
                                <tr>
                                    <td>
                                        <div class="d-inline-block align-middle">
                                            <img class="img-radius img-50 align-top m-r-15 rounded-circle" src="{{$order->user->image}}" alt="">
                                            <div class="d-inline-block">
                                                <p>{{$order->user->name}}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p>{{$order->date}}</p>
                                    </td>
                                    <td>
                                        <p>{{$order->time}}</p>
                                    </td>
                                    <td>
                                        <p>
                                            <a href="{{ route('orders.show',$order->id) }}" class="btn btn-info btn-sm"><i class="material-icons">show</i></a>
                                        </p>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="code-box-copy">
                            <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                            <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class="table-responsive sellers"&gt;
  &lt;table class="table table-bordernone"&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope="col"&gt;Name&lt;/th&gt;
        &lt;th scope="col"&gt;Sale&lt;/th&gt;
        &lt;th scope="col"&gt;Stock&lt;/th&gt;
        &lt;th scope="col"&gt;Categories&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/6.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/2.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/3.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/4.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/5.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/15.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@section('myjsfile')
    <script>
        var morris_chart = {
            init: function() {
                Morris.Area({
                    element: 'userGraph',
                    behaveLikeLine: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,0.05)",
                    scaleGridLineWidth: 0.2,
                    data: [
                            @foreach($numbers_of_users as $user)
                        {
                            x: '{{$user->date}}',
                            z: '{{$user->count}}'
                        },
                        @endforeach

                    ],
                    xkey: 'x',
                    ykeys: ['z'],
                    labels: ['Z'],
                    lineColors: [endlessAdminConfig.primary],
                })
            }
        };
        (function($) {
            "use strict";
            morris_chart.init()
        })(jQuery);
    </script>

    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'technicianGraph',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_technicians as $technician)

                { day: '{{$technician->date}}', value: '{{$technician->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>

    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'orderGraph',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_orders as $order)

                { day: '{{$order->date}}', value: '{{$order->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
@stop

@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>orders table</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">orders</li>
                                <li class="breadcrumb-item active">orders table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>orders data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>date</th>
                                        <th>time</th>
                                        <th>area</th>
                                        <th>block</th>
                                        <th>street</th>
                                        <th>house</th>
                                        <th>floor</th>
                                        <th>appartment</th>
                                        <th>city</th>
                                        <th>order number</th>
                                        <th>created at</th>
                                        <th>actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($orders as $key=>$order)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$order->date}}</td>
                                            <td>{{$order->time}}</td>
                                            <td>{{$order->area}}</td>
                                            <td>{{$order->block}}</td>
                                            <td>{{$order->street}}</td>
                                            <td>{{$order->house}}</td>
                                            <td>{{$order->floor}}</td>
                                            <td>{{$order->appartment}}</td>
                                            <td>{{$order->city->city_ar}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>
                                                <a href="{{ route('orders.edit',$order->id) }}" class="btn btn-info">edit</a>
                                                <a href="{{ route('orders.show',$order->id) }}" class="btn btn-success">details</a>
                                                <form id="delete-form-{{ $order->id }}" action="{{ route('orders.destroy',$order->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('are you sure ? you want to delete this field ?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $order->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">delete</button>

                                                <a href="{{route('orders.invoice',$order->id)}}" class="btn btn-warning" target="_blank">print invoice</a>

                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection


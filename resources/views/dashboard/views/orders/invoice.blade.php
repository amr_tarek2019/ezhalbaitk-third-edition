<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>invoice</title>
    <link rel="stylesheet" href="{{asset('assets/invoice/style.css')}}" media="all" />
</head>
<body onload="window.print();">
<header class="clearfix">
    <div id="logo">
        <img src="{{ asset($settings->image) }}" alt="ambeco">
    </div>
    <h1>Ezhalbaitk</h1>

    <div id="project">
        <div><span>رقم الفاتورة</span>{{$data->order_number}}</div>
        <div><span>اسم العميل</span> <span >{{$data->user->name}}</span></div>
        <div><span>التاريخ</span> {{$data->created_at}}</div>
        <div><span>رقم العميل</span> {{$data->user->id}}</div>
        <div><span>تليفون</span>{{$data->user->phone}}</div>
        <div><span>البريد الالكتروني</span>{{$data->user->email}}</div>
        <div><span>العنوان</span>{{$data->user->address}}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th>رقم الصنف</th>
            <th>البيان</th>
            <th class="service">الكمية</th>
            <th class="desc">سعر الوحدة / الريال</th>

        </tr>
        </thead>
        <tbody>

        <tr>
            <td class="service">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->id}}<br>
                @endforeach
            </td>
            <td class="desc">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->details_en}}<br>
                @endforeach
            </td>
            <td class="qty">
              @foreach($orderSubcategories as $orderSubcategory)
                    <br>{{$orderSubcategory->quantity}}<br>
              @endforeach
            </td>
            <td class="total">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->price}} {{$subcategory->currency}}<br>
                @endforeach
            </td>

        </tr>



        <tr>
            <td class="grand total">
             {{\App\OrderSubcategory::where('order_id',$data->id)->sum('total')}}
            </td>
<td colspan="7" class="grand total">الإجمالى </td>



</tr>



</tbody>
</table>

<div class="signature">
<div id="project">
<div><span>توقيع المسئول </span> ...................................................</div>
<div><span>البائع </span> ...................................................</div>
<div><span>التوقيع  </span>...................................................</div>

</div>
<div id="project2">
<div><span> المستلم </span> ...................................................</div>
<div><span> التوقيع</span> ...................................................</div>


</div>
</div>
</main>


</body>
</html>

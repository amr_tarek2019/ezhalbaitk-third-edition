@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>User Rate Details</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Rates</li>
                                <li class="breadcrumb-item">users</li>
                                <li class="breadcrumb-item active">User Rate Details</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>User Rate Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>order number</th>
                                        <th>rate</th>
                                        <th>feedback</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{$userRate->id}}</td>
                                    <td>{{$userRate->order->order_number}}</td>
                                    <td>{{$userRate->rate}}</td>
                                    <td>{{$userRate->review}}</td>
                                    <td>{{$userRate->created_at}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>User Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$userRate->user->id)}}" data-original-title="" title="">{{ $userRate->user->name}}</a></td>
                                    <td>{{ $userRate->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userRate->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userRate->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Technician Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$userRate->technician->user->id)}}" data-original-title="" title="">{{ $userRate->technician->user->name}}</a></td>
                                    <td>{{ $userRate->technician->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userRate->technician->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userRate->technician->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

@extends('dashboard.layouts.master')

@section('content')

    {{--<div class="col-lg-6 col-lg-offset-3">--}}
    {{--    <div class="panel">--}}
    {{--        <div class="panel-heading">--}}
    {{--            <h3 class="panel-title">{{__('Role Information')}}</h3>--}}
    {{--        </div>--}}

    {{--        <!--Horizontal Form-->--}}
    {{--        <!--===================================================-->--}}
    {{--        <form class="form-horizontal" action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">--}}
    {{--        	@csrf--}}
    {{--            <div class="panel-body">--}}
    {{--                <div class="form-group">--}}
    {{--                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>--}}
    {{--                    <div class="col-sm-9">--}}
    {{--                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="panel-heading">--}}
    {{--                    <h3 class="panel-title">{{ __('Permissions') }}</h3>--}}
    {{--                </div>--}}
    {{--                <div class="form-group">--}}
    {{--                    <label class="col-sm-3 control-label" for="banner"></label>--}}
    {{--                    <div class="col-sm-9">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Products') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="1">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Flash Deal') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="2">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Orders') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="3">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Sales') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="4">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Sellers') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="5">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Customers') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="6">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Messaging') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="7">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Business Settings') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="8">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Frontend Settings') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="9">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-10">--}}
    {{--                                <label class="control-label">{{ __('Staffs') }}</label>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-2">--}}
    {{--                                <label class="switch">--}}
    {{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="10">--}}
    {{--                                    <span class="slider round"></span>--}}
    {{--                                </label>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="panel-footer text-right">--}}
    {{--                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>--}}
    {{--            </div>--}}
    {{--        </form>--}}
    {{--        <!--===================================================-->--}}
    {{--        <!--End Horizontal Form-->--}}

    {{--    </div>--}}
    {{--</div>--}}




    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Role Information</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">Roles</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Add Role</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <form class="form-horizontal" action="{{ route('roles.update', $role->id) }}" method="POST" enctype="multipart/form-data">
                                    <input name="_method" type="hidden" value="PATCH">
                                    @csrf
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" value="{{ $role->name }}" placeholder="Name" id="name" name="name" class="form-control" required>                                </div>

                                        @php
                                            $permissions = json_decode($role->permissions);
                                        @endphp
                                        <div class="form-group">


                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-1"
                                                       @php if(in_array(1, $permissions)) echo "checked"; @endphp
                                                       value="1" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-1">users</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-2"
                                                       @php if(in_array(2, $permissions)) echo "checked"; @endphp
                                                       value="2" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-2">admins</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-3"
                                                       @php if(in_array(3, $permissions)) echo "checked"; @endphp
                                                       value="3" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-3">members</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-4"
                                                       @php if(in_array(4, $permissions)) echo "checked"; @endphp
                                                       value="4" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-4">technicians</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-5"
                                                       @php if(in_array(5, $permissions)) echo "checked"; @endphp
                                                       value="5" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-5">cities</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-6"
                                                       @php if(in_array(6, $permissions)) echo "checked"; @endphp
                                                       value="6" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-6">categories</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-7"
                                                       @php if(in_array(7, $permissions)) echo "checked"; @endphp
                                                       value="7" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-7">units</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-8"
                                                       @php if(in_array(8, $permissions)) echo "checked"; @endphp
                                                       value="8" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-8">subcategories</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-9"
                                                       @php if(in_array(9, $permissions)) echo "checked"; @endphp
                                                       value="9" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-9">packages</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-10"
                                                       @php if(in_array(10, $permissions)) echo "checked"; @endphp
                                                       value="10" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-10">subscriptions</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-11"
                                                       @php if(in_array(11, $permissions)) echo "checked"; @endphp
                                                       value="11" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-11">user subscriptions</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-12"
                                                       @php if(in_array(12, $permissions)) echo "checked"; @endphp
                                                       value="12" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-12">ratings</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-13"
                                                       @php if(in_array(13, $permissions)) echo "checked"; @endphp
                                                       value="13" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-13">orders</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-14"
                                                       @php if(in_array(14, $permissions)) echo "checked"; @endphp
                                                       value="14" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-14">staffs</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-15"
                                                       @php if(in_array(15, $permissions)) echo "checked"; @endphp
                                                       value="15" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-15">suggestions</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-16"
                                                       @php if(in_array(16, $permissions)) echo "checked"; @endphp
                                                       value="16" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-16">settings</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-17"
                                                       @php if(in_array(17, $permissions)) echo "checked"; @endphp
                                                       value="17" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-17">notifications</label>
                                            </div>

                                        </div>
                                        <button class="btn btn-primary" type="submit" data-original-title="" title="">Submit form</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

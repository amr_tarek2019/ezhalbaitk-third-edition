{{--@extends('dashboard.layouts.master')--}}

{{--@section('content')--}}

{{--<div class="row">--}}
{{--    <div class="col-sm-12">--}}
{{--        <a href="{{ route('roles.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Role')}}</a>--}}
{{--    </div>--}}
{{--</div>--}}

{{--<br>--}}

{{--<!-- Basic Data Tables -->--}}
{{--<!--===================================================-->--}}
{{--<div class="panel">--}}
{{--    <div class="panel-heading">--}}
{{--        <h3 class="panel-title">{{__('Roles')}}</h3>--}}
{{--    </div>--}}
{{--    <div class="panel-body">--}}
{{--        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">--}}
{{--            <thead>--}}
{{--                <tr>--}}
{{--                    <th width="10%">#</th>--}}
{{--                    <th>{{__('Name')}}</th>--}}
{{--                    <th width="10%">{{__('Options')}}</th>--}}
{{--                </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--                @foreach($roles as $key => $role)--}}
{{--                    <tr>--}}
{{--                        <td>{{$key+1}}</td>--}}
{{--                        <td>{{$role->name}}</td>--}}
{{--                        <td>--}}
{{--                            <div class="btn-group dropdown">--}}
{{--                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">--}}
{{--                                    {{__('Actions')}} <i class="dropdown-caret"></i>--}}
{{--                                </button>--}}
{{--                                <ul class="dropdown-menu dropdown-menu-right">--}}
{{--                                    <li><a href="{{route('roles.edit', encrypt($role->id))}}">{{__('Edit')}}</a></li>--}}
{{--                                    <li><a onclick="confirm_modal('{{route('roles.destroy', $role->id)}}');">{{__('Delete')}}</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}

{{--    </div>--}}
{{--</div>--}}

@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Roles DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">Roles DataTable</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('roles.create')}}" class="btn btn-primary">Add New</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Roles Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $key => $role)
                                                        <tr>
                                                            <td>{{$key+1}}</td>
                                                            <td>{{$role->name}}</td>
                                                            <td>
                                                                <a href="{{ route('roles.edit',encrypt($role->id))}}" class="btn btn-info btn-sm"><i class="material-icons">edit</i></a>

                                                                <form id="delete-form-{{ $role->id }}" action="{{ route('roles.destroy',$role->id) }}" style="display: none;" method="POST">
                                                                    @csrf
                                                                </form>
                                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('are you sure ? you want to delete this field ?')){
                                                                    event.preventDefault();
                                                                    document.getElementById('delete-form-{{ $role->id }}').submit();
                                                                    }else {
                                                                    event.preventDefault();
                                                                    }"><i class="material-icons">delete</i></button>
                                                            </td>

                                                        </tr>
                                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection



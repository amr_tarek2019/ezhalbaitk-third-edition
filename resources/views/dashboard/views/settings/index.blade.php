@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>settings</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">settings</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-12">
                        @include('dashboard.layouts.msg')
                        <form class="card" action="{{route('settings.update')}}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">settings</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">about E</label>
                                            <textarea class="form-control"
                                                      name="text_en" id="text_en"
                                                      rows="5" placeholder="Enter About your description">{{$settings->text_en}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br><br>

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">about A</label>
                                            <textarea class="form-control" rows="5"
                                                      name="text_ar" id="text_ar"
                                                      placeholder="Enter About your description">{{$settings->text_ar}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">facebook</label>
                                            <input class="form-control" type="text" name="facebook" id="facebook"
                                                      placeholder="Enter facebook"
                                                    value="{{$settings->facebook}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">google</label>
                                            <input class="form-control" type="text" name="google" id="google"
                                                   placeholder="Enter google"
                                                   value="{{$settings->google}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>

                                    <label class="form-label">logo</label>
                                    <img class="img-fluid rounded" src="{{ asset($settings->image) }}" itemprop="thumbnail" alt="gallery" data-original-title="" title=""
                                         style="margin-left: 15px;max-width: 100px;">

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">upload file</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="file" data-original-title="" title="" id="image" name="image" >
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection

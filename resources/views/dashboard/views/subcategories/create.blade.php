@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>Add Subcategory</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Subcategories</li>
                            <li class="breadcrumb-item active">Add Subcategory</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Complete Form</h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" action="{{ route('subcategories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">name english</label>
                                    <input class="form-control" id="name_en" name="name_en" placeholder="english name"/>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">name arabic</label>
                                    <input class="form-control" id="name_ar" name="name_ar"  placeholder="arabic name"/>
                                </div>
                            </div>
                            <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">english details</label>
                                        <textarea class="form-control" id="details_en" name="details_en" rows="3"></textarea>
                                    </div>
                                </div>

                            <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">arabic details</label>
                                        <textarea class="form-control" id="details_ar" name="details_ar" rows="3"></textarea>
                                    </div>
                                </div>

                            <br>
                            <div class="col-md-12">
                                <div class="col-form-label">unit</div>
                                <select name="unit" class="js-example-placeholder-multiple col-sm-12">
                                    @foreach($units as $unit)
                                        <option value="{{ $unit->id }}">{{ $unit->name_en }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">price</label>
                                    <input class="form-control" id="price" name="price"  placeholder="name"/>
                                </div>
                            </div>
                            <br>
                         <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>edit subscription</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">subscriptions</li>
                                <li class="breadcrumb-item active">edit subscription</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Complete Form</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('subscriptions.update',$subscription->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">months</label>
                                        <input class="form-control" type="number" value="{{$subscription->months}}" id="months" name="months" placeholder="months"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">price</label>
                                        <input class="form-control" value="{{$subscription->price}}" id="price" name="price"  placeholder="price"/>
                                    </div>
                                </div>
                                <br>

                                <div class="col-md-12">
                                    <div class="col-form-label">category</div>
                                    <select name="category" class="js-example-placeholder-multiple col-sm-12" multiple="multiple">
                                        @foreach($categories as $category)
                                            <option {{ $category->id == $subscription->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <br>
                                <div class="col-md-12">
                                    <div class="col-form-label">package</div>
                                    <select name="package" class="js-example-placeholder-multiple col-sm-12" multiple="multiple">
                                        @foreach($packages as $package)
                                            <option {{ $package->id == $subscription->package->id ? 'selected' : '' }} value="{{ $package->id }}">{{ $package->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

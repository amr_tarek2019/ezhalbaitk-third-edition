@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Create Technician</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Technicians</li>
                                <li class="breadcrumb-item active">Create Technician</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        <form class="card" method="POST" action="{{ route('technicians.store') }}">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">create Technician</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">name</label>
                                            <input class="form-control" name="name" id="name" type="text" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">email</label>
                                            <input class="form-control"  name="email" id="email" type="text" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">phone</label>
                                            <input class="form-control"  name="phone" id="phone" type="text" placeholder="phone">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">address</label>
                                            <input class="form-control"  name="address" id="address" type="text" placeholder="address">
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="col-form-label">category</div>
                                        <select name="category_id" id="category_id" class="js-example-placeholder-multiple col-sm-12" style="width: 1123.87px">
                                            @foreach($categories as $category)
                                                <option id="{{ $category->id }}" value="{{ $category->id }}">{{ $category->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
<br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="col-form-label">unit</div>
                                        <select name="unit_id" id="unit_id" class="js-example-placeholder-multiple col-sm-12" style="width: 1123.87px">
                                            @foreach($units as $unit)
                                                <option id="{{ $unit->id }}" value="{{ $unit->id }}">{{ $unit->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="col-form-label">subcategory</div>
                                        <select name="subcategory" id="subcategory_id"  class="js-example-placeholder-multiple col-sm-12" style="width: 1123.87px">
                                            @foreach($subcategories as $subcategory)
                                                <option value="{{ $subcategory->id }}">{{ $subcategory->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br><br><br><br>




                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Password</label>
                                            <input class="form-control" type="password" placeholder="Password" name="password" id="password">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">submit form</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection

@section('myjsfile')

    <script>


        $(document).on('change', '#category_id', function(){
            var category_id = $(this).val();
            //alert(company_id);
            if(category_id){
                $.ajax({
                    type:"GET",
                    // url:"{{url('get-category-units/')}}/?category_id="+category_id,
                    url:"/en/get-category-units/"+category_id,
                    success:function(res){
                        console.log(res);

                        if(res){
                            console.log(res);
                            $("#unit_id").empty();
                            $.each(res,function(key,value){
                                $("#unit_id").append('<option value="'+value+'" >'+key+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#unit_id").empty();
                        }
                    }
                });
            }else{
                $("#unit_id").empty();
            }
        });

    </script>


    <script>


        $(document).on('change', '#unit_id', function(){
            var unit_id = $(this).val();
            //alert(company_id);
            if(unit_id){
                $.ajax({
                    type:"GET",
                    // url:"{{url('get-subcategory-units/')}}/?category_id="+category_id,
                    url:"/en/get-subcategory-units/"+unit_id,
                    success:function(res){
                        console.log(res);

                        if(res){
                            console.log(res);
                            $("#subcategory_id").empty();
                            $.each(res,function(key,value){
                                $("#subcategory_id").append('<option value="'+value+'" >'+key+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#subcategory_id").empty();
                        }
                    }
                });
            }else{
                $("#subcategory_id").empty();
            }
        });

    </script>


@endsection

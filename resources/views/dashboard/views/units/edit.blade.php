@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit unit</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">units</li>
                                <li class="breadcrumb-item active">Edit unit</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Complete Form</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('units.update',$unit->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">english name</label>
                                        <input class="form-control" value="{{$unit->name_en}}" id="name_en" name="name_en" placeholder="Enter About your description"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">arabic name</label>
                                        <input class="form-control" value="{{$unit->name_ar}}" id="name_ar" name="name_ar"  placeholder="Enter About your description"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="col-form-label">Category</div>
                                    <select name="category" class="js-example-placeholder-multiple col-sm-12">
                                        @foreach($categories as $category)
                                            <option {{ $category->id == $unit->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <br>
                                <div style="margin-left: 15px;">
                                    <img class="img-responsive img-thumbnail" src="{{ asset($unit->image) }}" style="height: 100px; width: 100px;" alt="">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="form-label" style="margin-left: 30px;">Upload Image</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="image" name="image" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

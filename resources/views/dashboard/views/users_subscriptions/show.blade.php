@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>User Subscription Details</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item active">User Subscription Details</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>User Subscription Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>price</th>
                                        <th>date</th>
                                        <th>time</th>
                                        <th>subscription number</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{$userSubscription->id}}</td>
                                    <td>{{$userSubscription->total_price}}</td>
                                    <td>{{$userSubscription->date}}</td>
                                    <td>{{$userSubscription->time}}</td>
                                    <td>{{$userSubscription->subscription_number}}</td>
                                    <td>{{$userSubscription->created_at}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>User Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$userSubscription->user->id)}}" data-original-title="" title="">{{ $userSubscription->user->name}}</a></td>
                                    <td>{{ $userSubscription->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userSubscription->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userSubscription->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Technician Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <td> <a href="{{route('users.show',$userSubscription->technician($userSubscription->id)) }}" data-original-title="" title="">{{ $userSubscription->technician($userSubscription->id)->name }}</a></td>
                                    <td>{{ $userSubscription->technician($userSubscription->id)->email }}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userSubscription->technician($userSubscription->id)->image }}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userSubscription->technician($userSubscription->id)->phone }}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>category and package Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>category</th>
                                        <th>package</th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td>{{ $userSubscription->subscription->category->name_en}}</td>
                                    <td>{{ $userSubscription->subscription->package->name_en}}</td>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

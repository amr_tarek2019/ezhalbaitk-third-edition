<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('assets/dashboard/images/Untitled-1.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets/dashboard/images/Untitled-1.png') }}" type="image/x-icon">
    <title>Ezhalbaitk</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }
        .full-height {
            height: 100vh;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }
        .content {
            text-align: center;
        }
        .title {
            font-size: 84px;
        }
        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>


    <div class="content">
        <div class="title m-b-md">
            <img src="{{ asset('assets/dashboard/images/landing-page.png') }}" width="100%">

        </div>

    </div>

    <!-- Footer -->
    <footer id="footer" style="
    margin-bottom: 25px;
">
        <div class="container">

            <!-- footer socials -->
            <div class="row">

                <div class="footer_socials col-sm-12 text-center">

                    <div class="contact_icons">

                    </div>

                    <div class=“site-copyright”>
                        <p align="center"> <a href="http://2grand.net/" target=“_blank” class=“grand”><img src="{{ asset('assets/dashboard/images/grandandroid.png') }}" width="100px"></a> جميع الحقوق محفوظة لشركة جراند ©</p>
                    </div>
                </div>

            </div>
            <!-- end footer socials -->

        </div>
        <!-- end container -->
    </footer>
    <!-- end footer -->

</body>
</html>

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



// user routes app

Route::group(['prefix'=>'user','namespace'=>'Api\User'], function (){
    Route::group(['prefix'=>'About'],function () {
        Route::get('', 'AboutController@AboutForm');
    });

    Route::group(['prefix'=>'Categories'],function () {
        Route::get('', 'CategoriesController@CategoryForm');
    });

    Route::group(['prefix'=>'Cities'],function () {
        Route::get('', 'CitiesController@CityForm');
    });

    Route::group(['prefix'=>'Subcategories'],function () {
        Route::get('', 'SubcategoriesController@SubcategoriesForm');
    });

    Route::group(['prefix'=>'Contacts'],function () {
        Route::post('', 'ContactsController@createContacts');
    });

    Route::group(['prefix'=>'Packages'],function () {
        Route::get('', 'PackagesController@PackagesForm');
    });

    Route::group(['prefix'=>'Profile'],function () {
        Route::get('GetData', 'ProfileController@GetProfileDataForm');
        Route::post('Update', 'ProfileController@updateProfile');
        Route::post('GetPassword', 'ProfileController@getPasswordProfile');
        Route::post('UpdatePassword', 'ProfileController@updatePasswordProfile');
    });

    Route::group(['prefix'=>'Authentication'],function () {
        Route::post('Registration', 'AuthenticationController@registration');
        Route::post('Login','AuthenticationController@login');
        Route::post('Location','AuthenticationController@location');
        Route::post('ForgetPassword', 'AuthenticationController@forgetPassword');
        Route::post('VerificationCode', 'AuthenticationController@verification');
        Route::post('ChangePassword', 'AuthenticationController@changePassword');
        Route::post('SocialLogin', 'AuthenticationController@socialLogin');
        Route::post('GetVerificationCode', 'AuthenticationController@getVerfificationCode');
    });

    Route::group(['prefix'=>'Subscriptions'],function () {
        Route::get('', 'SubscriptionsController@subscriptionsForm');
        Route::post('Reserve', 'SubscriptionsController@createSubscription');
        Route::get('UserShow', 'SubscriptionsController@showUserSubscriptions');
        Route::post('Update', 'SubscriptionsController@updateSubscription');
        Route::get('NextVisit', 'SubscriptionsController@subscriptionNextVisitStatus');
        Route::get('InProgress', 'SubscriptionsController@subscriptionInProgressStatus');
        Route::get('Completed', 'SubscriptionsController@subscriptionCompletedStatus');
        Route::post('RateTechnician', 'SubscriptionsController@rateTechnician');
    });

    Route::group(['prefix'=>'Order'],function () {
        Route::post('','OrderController@createOrder');
        Route::get('Status','OrderController@orderStatus');
        Route::get('JobDetails','OrderController@JobDetails');
        Route::post('Reschedule','OrderController@edit');
        Route::post('Cancel','OrderController@destroy');

    });

    Route::group(['prefix'=>'Rate'],function () {
        Route::post('Technician','RateTechnicianController@createRate');
    });

    Route::group(['prefix'=>'Notifications'],function () {
        Route::get('','NotificationsController@notificationsForm');
    });

    Route::group(['prefix'=>'History'],function () {
        Route::get('', 'HistoryController@index');
        Route::get('ActiveJobs','HistoryController@ActiveJobs');
          Route::get('ItemDetails','HistoryController@show');
    });

    Route::group(['prefix'=>'Units'],function () {
        Route::get('', 'UnitsController@unitsForm');
    });

    Route::group(['prefix'=>'Filter'],function () {
        Route::post('', 'FilterationController@FilterForm');
    });
});





Route::group(['prefix'=>'technician','namespace'=>'Api\Technician'], function (){
    Route::group(['prefix'=>'Authentication'],function () {
        Route::post('Login','AuthenticationController@login');
        Route::post('ForgetPassword', 'AuthenticationController@forgetPassword');
        Route::post('VerificationCode', 'AuthenticationController@verification');
        Route::post('Location','AuthenticationController@location');

    });
    Route::group(['prefix'=>'About'],function () {
        Route::get('', 'AboutController@AboutForm');
    });
    Route::group(['prefix'=>'Contacts'],function () {
        Route::post('', 'ContactsController@createContacts');
    });

    Route::group(['prefix'=>'Profile'],function () {
        Route::get('GetData', 'ProfileController@GetProfileDataForm');
        Route::post('Update', 'ProfileController@updateProfile');
        Route::post('GetPassword', 'ProfileController@getPasswordProfile');
        Route::post('UpdatePassword', 'ProfileController@updatePasswordProfile');
    });
    Route::group(['prefix'=>'Notifications'],function () {
        Route::get('','NotificationsController@notificationsForm');
    });

    Route::group(['prefix'=>'MyOrders'],function () {
        Route::get('NextVisit', 'TechnicianOrdersController@nextVisit');
        Route::get('InProgress', 'TechnicianOrdersController@inProgress');
        Route::get('Completed', 'TechnicianOrdersController@completed');
        Route::get('OrderDetails', 'TechnicianOrdersController@orderDetails');
        Route::get('AcceptOrder', 'TechnicianOrdersController@acceptOrderForm');
        Route::post('ReplyOnOrder', 'TechnicianOrdersController@replyOnOrder');
        Route::post('ChangeOrderStatus', 'TechnicianOrdersController@changeOrderStatus');
    });

    Route::group(['prefix'=>'OrderNote'],function () {
        Route::post('', 'TechnicianNoteController@createNote');
        // Route::post('Update', 'TechnicianNoteController@updateNote');
        Route::get('WaitingOrders', 'TechnicianNoteController@waitingOrders');
    });

    Route::group(['prefix'=>'EnterPriceAmount'],function () {
        Route::post('', 'OrderPriceAmountController@enterPriceAmount');
    });

    Route::group(['prefix'=>'Rate'],function () {
        Route::post('Order','RateOrderController@store');
    });

});

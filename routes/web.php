<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/get-category-units/{category_id}', 'Dashboard\Admin\CategoryController@getCategoryUnits');

Route::get('/get-subcategory-units/{unit_id}', 'Dashboard\Admin\CategoryController@getSubcategoriesUnit');

Route::get('/', function () {
    return view('welcome');
});


//Route::get('/login', function () {
//    return view('login');
//});
//
//Route::get('/dashboard', function () {
//    return view('dashboard');
//});



Route::group(['prefix'=>'admin','namespace'=>'Dashboard\Auth'], function (){
    Route::group(['prefix'=>'login'],function () {
        Route::get('', 'AuthenticationController@index')->name('login');
        Route::post('auth', 'AuthenticationController@login')->name('user.login');
        Route::get('/show/{id}','SuggestionsController@show')->name('suggestion.show');
        Route::get('/show/{id}','OrdersController@show')->name('order.show');
    });
});

Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'Dashboard\Admin'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/home', 'DashboardController@index')->name('dashboard');
        Route::post('', 'DashboardController@logout')->name('logout');
        Route::get('/backup','DashboardController@backup')->name('backup');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('', 'ProfileController@index')->name('profile');
        Route::post('/update/{id}','ProfileController@update')->name('profile.update');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('', 'SettingsController@index')->name('settings');
        Route::post('/update','SettingsController@update')->name('settings.update');
    });

    Route::prefix('suggestions')->group(function (){
        Route::get('','SuggestionsController@index')->name('suggestions.index');
        Route::get('/show/{id}','SuggestionsController@show')->name('suggestions.show');
        Route::post('/{id}','SuggestionsController@destroy')->name('suggestions.destroy');
    });

    Route::prefix('users')->group(function (){
        Route::get('','UsersController@index')->name('users.index');
        Route::get('/create','UsersController@create')->name('users.create');
        Route::post('/store','UsersController@store')->name('users.store');
        Route::get('/details/{id}','UsersController@show')->name('users.show');
        Route::get('/edit/{id}','UsersController@edit')->name('users.edit');
        Route::post('/update/{id}','UsersController@update')->name('users.update');
        Route::post('/{id}','UsersController@destroy')->name('users.destroy');
        Route::post('/status/{id}', 'UsersController@updateStatus')->name('users.status');
    });


    Route::prefix('cities')->group(function (){
        Route::get('','CityController@index')->name('cities.index');
        Route::get('/create','CityController@create')->name('cities.create');
        Route::post('/store','CityController@store')->name('cities.store');
        Route::get('/edit/{id}','CityController@edit')->name('cities.edit');
        Route::post('/update/{id}','CityController@update')->name('cities.update');
        Route::post('/{id}','CityController@destroy')->name('cities.destroy');
        Route::post('/status/{id}', 'CityController@updateStatus')->name('cities.status');
    });

    Route::prefix('categories')->group(function (){
        Route::get('','CategoryController@index')->name('categories.index');
        Route::get('/create','CategoryController@create')->name('categories.create');
        Route::post('/store','CategoryController@store')->name('categories.store');
        Route::get('/edit/{id}','CategoryController@edit')->name('categories.edit');
        Route::post('/update/{id}','CategoryController@update')->name('categories.update');
        Route::post('/{id}','CategoryController@destroy')->name('categories.destroy');
        Route::post('/status/{id}', 'CategoryController@updateStatus')->name('categories.status');
    });

    Route::prefix('units')->group(function (){
        Route::get('','UnitController@index')->name('units.index');
        Route::get('/create','UnitController@create')->name('units.create');
        Route::post('/store','UnitController@store')->name('units.store');
        Route::get('/edit/{id}','UnitController@edit')->name('units.edit');
        Route::post('/update/{id}','UnitController@update')->name('units.update');
        Route::post('/{id}','UnitController@destroy')->name('units.destroy');
        Route::post('/status/{id}', 'UnitController@updateStatus')->name('units.status');
    });

    Route::prefix('subcategories')->group(function (){
        Route::get('','SubcategoryController@index')->name('subcategories.index');
        Route::get('/create','SubcategoryController@create')->name('subcategories.create');
        Route::post('/store','SubcategoryController@store')->name('subcategories.store');
        Route::get('/edit/{id}','SubcategoryController@edit')->name('subcategories.edit');
        Route::post('/update/{id}','SubcategoryController@update')->name('subcategories.update');
        Route::post('/{id}','SubcategoryController@destroy')->name('subcategories.destroy');
        Route::post('/status/{id}', 'SubcategoryController@updateStatus')->name('subcategories.status');
    });


    Route::prefix('packages')->group(function (){
        Route::get('','PackagesController@index')->name('packages.index');
        Route::get('/create','PackagesController@create')->name('packages.create');
        Route::post('/store','PackagesController@store')->name('packages.store');
        Route::get('/edit/{id}','PackagesController@edit')->name('packages.edit');
        Route::post('/update/{id}','PackagesController@update')->name('packages.update');
        Route::post('/{id}','PackagesController@destroy')->name('packages.destroy');
        Route::post('/status/{id}', 'PackagesController@updateStatus')->name('packages.status');
    });

    Route::prefix('subscriptions')->group(function (){
        Route::get('','SubscriptionsController@index')->name('subscriptions.index');
        Route::get('/create','SubscriptionsController@create')->name('subscriptions.create');
        Route::post('/store','SubscriptionsController@store')->name('subscriptions.store');
        Route::get('/edit/{id}','SubscriptionsController@edit')->name('subscriptions.edit');
        Route::post('/update/{id}','SubscriptionsController@update')->name('subscriptions.update');
        Route::post('/{id}','SubscriptionsController@destroy')->name('subscriptions.destroy');
        Route::post('/status/{id}', 'SubscriptionsController@updateStatus')->name('subscriptions.status');
    });

    Route::prefix('users')->group(function (){
        Route::get('','UsersController@index')->name('users.index');
        Route::get('/details/{id}','UsersController@show')->name('users.show');
        Route::post('/{id}','UsersController@destroy')->name('users.destroy');
        Route::post('/status/{id}', 'UsersController@updateStatus')->name('users.status');
    });

    Route::prefix('admins')->group(function (){
        Route::get('','AdminsController@index')->name('admins.index');
        Route::get('/create','AdminsController@create')->name('admins.create');
        Route::post('/store','AdminsController@store')->name('admins.store');
        Route::get('/edit/{id}','AdminsController@edit')->name('admins.edit');
        Route::post('/update/{id}','AdminsController@update')->name('admins.update');
        Route::post('/{id}','AdminsController@destroy')->name('admins.destroy');
        Route::post('/status/{id}', 'AdminsController@updateStatus')->name('admins.status');
    });

    Route::prefix('members')->group(function (){
        Route::get('','MembersController@index')->name('members.index');
        Route::get('/create','MembersController@create')->name('members.create');
        Route::post('/store','MembersController@store')->name('members.store');
        Route::get('/edit/{id}','MembersController@edit')->name('members.edit');
        Route::post('/update/{id}','MembersController@update')->name('members.update');
        Route::post('/{id}','MembersController@destroy')->name('members.destroy');
        Route::post('/status/{id}', 'MembersController@updateStatus')->name('members.status');
    });

    Route::prefix('technicians')->group(function (){
        Route::get('','TechniciansController@index')->name('technicians.index');
        Route::get('/create','TechniciansController@create')->name('technicians.create');
        Route::post('/store','TechniciansController@store')->name('technicians.store');
        Route::get('/edit/{id}','TechniciansController@edit')->name('technicians.edit');
        Route::post('/update/{id}','TechniciansController@update')->name('technicians.update');
        Route::post('/{id}','TechniciansController@destroy')->name('technicians.destroy');
        Route::post('/status/{id}', 'TechniciansController@updateStatus')->name('technicians.status');
        Route::post('/status/availability/{id}', 'TechniciansController@technicianAvailability')->name('technicians.availability');
    });

    Route::prefix('rates')->group(function (){
        Route::get('users','UsersRatesController@index')->name('users.rates.index');
        Route::get('users/show/{id}','UsersRatesController@show')->name('users.rates.show');
        Route::post('users/{id}','UsersRatesController@destroy')->name('users.rates.destroy');
    });

    Route::prefix('rates')->group(function (){
        Route::get('technicians','TechniciansRateController@index')->name('technicians.rates.index');
        Route::get('technicians/show/{id}','TechniciansRateController@show')->name('technicians.rates.show');
        Route::post('technicians/{id}','TechniciansRateController@destroy')->name('technicians.rates.destroy');
    });

    Route::prefix('subscriptions-users')->group(function (){
        Route::get('','UsersSubscriptionsController@index')->name('users.subscriptions.index');
        Route::get('/show/{id}','UsersSubscriptionsController@show')->name('users.subscriptions.show');
        Route::post('/{id}','UsersSubscriptionsController@destroy')->name('users.subscriptions.destroy');
        Route::post('/status/{id}', 'UsersSubscriptionsController@updateStatus')->name('users.subscriptions.status');
    });

    Route::prefix('orders')->group(function (){
        Route::get('','OrdersController@index')->name('orders.index');
        Route::get('/edit/{id}','OrdersController@edit')->name('orders.edit');
        Route::post('/update/{id}','OrdersController@update')->name('orders.update');
        Route::post('/{id}','OrdersController@destroy')->name('orders.destroy');
        Route::get('/details/{id}','OrdersController@show')->name('orders.show');
        Route::get('/invoice/{id}','OrdersController@generatePdf')->name('orders.invoice');

    });

    Route::resource('roles','RoleController');
    Route::post('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');

    Route::resource('staffs','StaffController');
    Route::post('/staffs/destroy/{id}', 'StaffController@destroy')->name('staffs.destroy');

    Route::prefix('notifications')->group(function (){
        Route::get('','NotificationsController@index')->name('notifications.index');
        Route::get('/create','NotificationsController@create')->name('notifications.create');
        Route::post('/store','NotificationsController@store')->name('notifications.store');
        Route::post('/{id}','NotificationsController@destroy')->name('notifications.destroy');
    });

});
